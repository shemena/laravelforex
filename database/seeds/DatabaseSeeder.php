<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * List of seeds
     * @var array
     */
    protected static $seeds = [
        \Database\Seeds\Custom\UsersTableSeeder::class,
        \Database\Seeds\Custom\PagesTableSeeder::class,
        \Database\Seeds\Custom\WikiTableSeeder::class,
        \Database\Seeds\Custom\MenusSeeder::class,
        \Database\Seeds\Custom\SettingsTableSeeder::class,
        LocalizationsTableSeeder::class,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            DB::beginTransaction();
            $this->seed();
            DB::commit();
        }catch (Exception $exception){
            DB::rollback();
            $this->command->info($exception->getMessage());
        }
    }

    /**
     * Massive seeding
     */
    protected function seed()
    {
        foreach (self::$seeds as $seed){
            if(class_exists($seed)){
                $this->call($seed);
            }else{
                throw new Exception("Seeder class {$seed} not found...");
            }
        }
    }
}
