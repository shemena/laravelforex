<?php
namespace Database\Seeds\Custom;

use App\Models\Wiki;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Page;
use App\Models\Translation;

class WikiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $author = \App\Models\User::first();

        foreach ($this->getStub() as $wiki) {

            // Creating model
            $model = \App\Models\Wiki::updateOrCreate(
                ['slug' => $wiki['slug']],
                [
                    'term'            => $wiki['term']['en'],
                    'content'         => $wiki['content']['en'],
                    'status'          => true,
                    'author_id'       => !empty($author) ? $author->id : '',
                    'seo_title'       => $wiki['term']['en'],
                    'seo_description' => $wiki['seo_description']['en'],
                    'no_index'        => false,
                    'no_follow'       => false,
                ]
            );

            // Creating translation of 'term' field for each lang
            foreach ($wiki['term'] as $lang => $title) {
                if($lang!='en') {
                    Translation::updateOrCreate(
                        [
                            'table_name'  => $model->getTable(),
                            'column_name' => 'term',
                            'foreign_key' => $model->id,
                            'locale'      => $lang
                        ],
                        ['value' => $title]
                    );
                    Translation::updateOrCreate(
                        [
                            'table_name'  => $model->getTable(),
                            'column_name' => 'seo_title',
                            'foreign_key' => $model->id,
                            'locale'      => $lang
                        ],
                        ['value' => $title]
                    );
                    Translation::updateOrCreate(
                        [
                            'table_name'  => $model->getTable(),
                            'column_name' => 'slug',
                            'foreign_key' => $model->id,
                            'locale'      => $lang,
                        ],
                        ['value' => $wiki['slug']]
                    );
                }
            }

            // Creating translation of 'seo_description' field for each lang
            foreach ($wiki['seo_description'] as $lang => $seo_description) {
                if($lang!='en') {
                    Translation::updateOrCreate(
                        [
                            'table_name'  => $model->getTable(),
                            'column_name' => 'seo_description',
                            'foreign_key' => $model->id,
                            'locale'      => $lang
                        ],
                        ['value' => $seo_description]
                    );
                }
            }

            // Creating translation of 'content' field for each lang
            foreach ($wiki['content'] as $lang => $content) {
                if($lang != 'en') {
                    Translation::updateOrCreate(
                        [
                            'table_name'  => $model->getTable(),
                            'column_name' => 'content',
                            'foreign_key' => $model->id,
                            'locale'      => $lang,
                        ],
                        ['value' => $content]
                    );
                }
            }
        }
    }

    /**
     * Getting stub file by filename
     * @param string $filename
     * @return mixed
     */
    protected function getStub($filename = 'wikis.php')
    {
        $file = resource_path("stubs/" . $filename);

        if (file_exists($file)) {
            return require_once($file);
        } else {
            echo "Stub file {$file} doesn\'t exist";
            exit();
        }
    }
}
