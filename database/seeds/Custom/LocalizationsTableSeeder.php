<?php

use Illuminate\Database\Seeder;

class LocalizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getStub() as $localization) {

            // Creating model
            $model = \App\Models\Localization::firstOrCreate(
                [
                    'iso_code' => $localization['iso_code'],
                    'name'     => $localization['name'],
                    'script'   => $localization['script'],
                    'native'   => $localization['native'],
                    'regional' => $localization['regional'],
                    'enable'   => $localization['enable'],
                    'status'   => $localization['status'],
                    'alphabet' => $localization['alphabet'],
                ]
            );
        }
    }

    protected function getStub($filename = 'localizations.php') : array
    {
        $file = resource_path("stubs/" . $filename);

        if (file_exists($file)) {
            return require($file);
        } else {
            echo "Stub file {$file} doesn\'t exist";
            exit();
        }
    }

}
