<?php
namespace Database\Seeds\Custom;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('settings')->truncate();

        foreach ($this->getTabs() as $tabName => $tabData) {
            foreach ($tabData['items'] as $item) {
                Setting::firstOrcreate([
                    'key'          => $tabData['key'] . '.' . $item['key'],
                    'display_name' => $item['display_name'],
                    'value'        => $item['value'],
                    'details'      => null,
                    'type'         => $item['type'],
                    'order'        => $item['order'],
                    'group'        => $tabName,
                ]);
            }
        }
    }

    /**
     * Getting array of data for setting tabs
     * @return array
     */
    protected function getTabs()
    {
        return [
            'Admin Panel'     => [
                'key'   => 'admin',
                'items' => [
                    [
                        'key'          => 'title',
                        'display_name' => 'Title',
                        'value'        => config('app.name'),
                        'type'         => 'text',
                        'order'        => 1,
                    ],
                    [
                        'key'          => 'description',
                        'display_name' => 'Description',
                        'value'        => null,
                        'type'         => 'text',
                        'order'        => 2,
                    ],
                    [
                        'key'          => 'loader',
                        'display_name' => 'Loader',
                        'value'        => null,
                        'type'         => 'image',
                        'order'        => 3,
                    ],
                    [
                        'key'          => 'logo',
                        'display_name' => 'Logo',
                        'value'        => null,
                        'type'         => 'image',
                        'order'        => 4,
                    ],
                    [
                        'key'          => 'background',
                        'display_name' => 'Background',
                        'value'        => null,
                        'type'         => 'image',
                        'order'        => 4,
                    ],
                ],
            ],
            'Site'            => [
                'key'   => 'site',
                'items' => [
                    [
                        'key'          => 'title',
                        'display_name' => 'Title',
                        'value'        => config('app.name'),
                        'type'         => 'text',
                        'order'        => 1,
                    ],
                    [
                        'key'          => 'description',
                        'display_name' => 'Description',
                        'value'        => null,
                        'type'         => 'text',
                        'order'        => 2,
                    ],
                    [
                        'key'          => 'logo',
                        'display_name' => 'Logo(main)',
                        'value'        => null,
                        'type'         => 'image',
                        'order'        => 3,
                    ],
                    [
                        'key'          => 'logo-footer',
                        'display_name' => 'Logo(footer)',
                        'value'        => null,
                        'type'         => 'image',
                        'order'        => 4,
                    ],
                    [
                        'key'          => 'phone-number',
                        'display_name' => 'Phone number(main)',
                        'value'        => null,
                        'type'         => 'text',
                        'order'        => 5,
                    ],
                    [
                        'key'          => 'adress',
                        'display_name' => 'Adress',
                        'value'        => null,
                        'type'         => 'text',
                        'order'        => 6,
                    ],
                    [
                        'key'          => 'support-email',
                        'display_name' => 'Support E-mail',
                        'value'        => 'support@' . last(explode('//', config('app.url'))),
                        'type'         => 'text',
                        'order'        => 7,
                    ],
                    [
                        'key'          => 'terms-conditions',
                        'display_name' => 'Terms & Conditions',
                        'value'        => null,
                        'type'         => 'text',
                        'order'        => 8,
                    ],
                ],
            ],
            'Social Networks' => [
                'key'   => 'social',
                'items' => [
                    [
                        'key'          => 'twitter',
                        'display_name' => 'Twitter',
                        'value'        => 'https://twitter.com',
                        'type'         => 'text',
                        'order'        => 1,
                    ],
                    [
                        'key'          => 'vk',
                        'display_name' => 'Vkontakte',
                        'value'        => 'https://vk.com',
                        'type'         => 'text',
                        'order'        => 2,
                    ],
                    [
                        'key'          => 'facebook',
                        'display_name' => 'Facebook',
                        'value'        => 'https://www.facebook.com/',
                        'type'         => 'text',
                        'order'        => 3,
                    ],
                    [
                        'key'          => 'google',
                        'display_name' => 'Google+',
                        'value'        => 'https://plus.google.com',
                        'type'         => 'text',
                        'order'        => 4,
                    ],
                ],
            ],
            'Messengers'      => [
                'key'   => 'messengers',
                'items' => [
                    [
                        'key'          => 'telegram',
                        'display_name' => 'Telegram',
                        'value'        => null,
                        'type'         => 'text',
                        'order'        => 1,
                    ],
                    [
                        'key'          => 'whats-app',
                        'display_name' => 'Whats App',
                        'value'        => null,
                        'type'         => 'text',
                        'order'        => 2,
                    ],
                    [
                        'key'          => 'facebook',
                        'display_name' => 'Facebook Messenger',
                        'value'        => null,
                        'type'         => 'text',
                        'order'        => 3,
                    ],
                    [
                        'key'          => 'viber',
                        'display_name' => 'Viber',
                        'value'        => null,
                        'type'         => 'text',
                        'order'        => 4,
                    ],
                    [
                        'key'          => 'skype',
                        'display_name' => 'Skype',
                        'value'        => null,
                        'type'         => 'text',
                        'order'        => 5,
                    ],
                ],
            ],
        ];
    }
}
