<?php
namespace Database\Seeds\Custom;

use App\Models\Role;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;

class AdminUsersSeeder extends Seeder
{
    protected static $permissions = [
        'admin'     => 'all',
        'affiliate' => [
            'browse' => [
                'admin',
                'leads',
                'demo_leads',
                'landings',
            ],
            'read'   => [
                'leads',
                'demo_leads',
                'landings',
            ],
        ],
        'content'   => [
            'browse' => [
                'admin',
                'menus',
                'news',
                'faq',
                'promotions',
                'documents',
                'wiki',
                'manuals',
                'pages',
                'sliders',
                'settings',
                'localizations'
            ],
            'read'   => [
                'menus',
                'news',
                'faq',
                'promotions',
                'documents',
                'wiki',
                'manuals',
                'pages',
                'sliders',
                'settings',
            ],
            'edit'   => [
                'news',
                'faq',
                'promotions',
                'documents',
                'wiki',
                'manuals',
                'pages',
                'sliders',
                'settings',
                'localizations'
            ],
            'add'    => [
                'news',
                'faq',
                'promotions',
                'documents',
                'wiki',
                'manuals',
                'pages',
                'sliders',
                'settings',
            ],
            'delete' => [
                'news',
                'faq',
                'promotions',
                'wiki',
                'manuals',
                'pages',
            ],
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->bindPermissions();
        $this->command->info("Roles binding finished");
    }

    /**
     * @param Role $role
     * @param Collection $permissions
     */
    protected function bindPermissions()
    {
        foreach (self::$permissions as $roleName => $permissions){
            $role = Role::where('name', $roleName)->firstOrFail();
            if(is_array($permissions)){

                foreach ($permissions as $action => $entities){
                    foreach ($entities as $entity){
                        $permission = Permission::where('key', $action . '_' . $entity)->firstOrFail();
                        $role->permissions()->attach($permission->id);
                    }
                }

            }else{
                $all_permissions = Permission::all();
                $role->permissions()->sync($all_permissions->pluck('id')->all());
            }
        }
    }
}