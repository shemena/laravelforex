<?php
namespace Database\Seeds\Custom;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Page;
use App\Models\Translation;

class PagesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     * @return void
     */
    public function run()
    {
        $menuItems = $this->getStub('HeaderMenu.php');

        foreach ($menuItems as $menuItem){
            $this->createModel($menuItem);
        }
    }

    /**
     * Create page model
     * @param array $stub
     */
    protected function createModel(array $stub)
    {
        $author = \App\Models\User::first();
        $slug = $this->getSlug($stub);

        if(!empty($slug)){
            $page = Page::firstOrCreate([
                'title'           => $stub['title']['en'],
                'slug'            => $this->getSlug($stub),
                'content'         => null,
                'status'          => true,
                'image'           => null,
                'image_alt'       => null,
                'author_id'       => !empty($author) ? $author->id : '',
                'seo_title'       => $stub['title']['en'],
                'seo_description' => null,
                'no_index'        => false,
                'no_follow'       => false,
            ]);
            $this->createTranslations($stub['title'], $page);

            if(!empty($stub['childrens'])){
                foreach ($stub['childrens'] as $stub){
                    $this->createModel($stub);
                }
            }
        }
    }

    /**
     * Getting slug from stub data
     * @param array $stub
     * @return mixed
     */
    protected function getSlug(array $stub)
    {
        $parameters = json_decode($stub['parameters'], true);
        return $parameters['page'];
    }

    /**
     * Create translations for column 'slug' and 'title'
     * @param array $titles
     * @param Page $model
     */
    protected function createTranslations(array $titles, Page $model)
    {
        foreach ($titles as $lang => $title) {
            Translation::firstOrCreate([
                'table_name'  => $model->getTable(),
                'column_name' => 'title',
                'foreign_key' => $model->id,
                'locale'      => $lang,
                'value'       => $title,
            ]);
            Translation::firstOrCreate([
                'table_name'  => $model->getTable(),
                'column_name' => 'slug',
                'foreign_key' => $model->id,
                'locale'      => $lang,
                'value'       => $model->slug,
            ]);
        }
    }

    /**
     * Getting stub file by filename
     * @param string $filename
     * @return mixed
     */
    protected function getStub($filename = 'pages.php')
    {
        $file = resource_path("stubs/" . $filename);

        if(file_exists($file)){
            return require($file);
        }else{
            echo "Stub file {$file} doesn\'t exist";
            exit();
        }
    }
}
