<?php
namespace Database\Seeds\Custom;

use App\Models\Menu;
use App\Models\MenuItem;
use App\Models\Translation;
use Illuminate\Database\Seeder;

class MenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menuHeader = Menu::firstOrCreate([
            'name' => 'header',
        ]);
        $this->addItemsfor($menuHeader);

        $menuFooter = Menu::firstOrCreate([
            'name' => 'footer',
        ]);
        $this->addItemsfor($menuFooter);
    }

    /**
     * Adding menu items for incoming menu
     * @param Menu $menu
     */
    protected function addItemsfor(Menu $menu)
    {
        $stubs = $this->getStubsFrom(ucfirst($menu->name) .  'Menu.php');

        if(!empty($stubs)){
            foreach ($stubs as $stub){
                $this->createFromStub($stub, $menu);
            }
        }else{
            $this->command->info("There is no stubs in file stubs/Menu.php");
        }
    }

    /**
     * Getting array of stubs from stub file
     * @param string $fileName
     * @return mixed
     */
    protected function getStubsFrom(string $fileName)
    {
        $file = resource_path("stubs/" . $fileName);
        if (file_exists($file)) {
            return require($file);
        } else {
            echo "Stub file {$file} doesn\'t exist";
        }
    }

    /**
     * Create MenuItem model from stub array
     * @param array $stub
     * @param Menu $menu
     * @param null $parent_id
     */
    protected function createFromStub(array $stub, Menu $menu, $parent_id = null)
    {
        //Create menu item for incoming menu
        $menuItem = MenuItem::firstOrCreate([
            'menu_id'    => $menu->id,
            'title'      => $stub['title']['en'],
            'url'        => $stub['url'],
            'target'     => $stub['target'],
            'icon_class' => $stub['icon_class'],
            'color'      => $stub['color'],
            'parent_id'  => $parent_id,
            'order'      => $stub['order'],
            'route'      => $stub['route'],
            'parameters' => $stub['parameters'],
        ]);

        // Create translation for field 'title'
        $this->createTranslationFor('title', $stub, $menuItem);

        // Create child items for this menu item
        if(!empty($stub['childrens'])){
            foreach ($stub['childrens'] as $childItem){
                $this->createFromStub($childItem, $menu, $menuItem->id);
            }
        }
    }

    /**
     * Create translations for each menu item
     * @param array $columns
     * @param MenuItem $model
     * @return \Illuminate\Support\Collection
     */
    protected function createTranslationFor(string $field, array $stub, MenuItem $model)
    {
        foreach ($stub[$field] as $lang => $title) {
            Translation::firstOrCreate([
                'table_name'  => $model->getTable(),
                'column_name' => $field,
                'foreign_key' => $model->id,
                'locale'      => $lang,
                'value'       => $title,
            ]);
        }
    }
}