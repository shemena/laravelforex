<?php
namespace Database\Seeds\Custom;

use Illuminate\Database\Seeder;
use App\Models\Menu;
use App\Models\MenuItem;

class FooterMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::firstOrCreate([
            'name' => 'footer',
        ]);
        $menuItems = $this->getStub()['headerMenu'];
        foreach ($menuItems as $menuItem){

            $title = $menuItem['title']['en'];
            $page = \App\Models\Page::where('title', $title)->first();
            $parent = MenuItem::firstOrCreate([
                'menu_id'    => $menu->id,
                'title'      => $title,
                'url'        => '',
                'target'     => '_self',
                'icon_class' => null,
                'color'      => null,
                'parent_id'  => null,
                'order'      => 1,
                'route'      => !empty($page) ? 'page.one' : mb_strtolower($title),
                'parameters' => !empty($page) ? "{\"page\" : \"{$page->slug}\"}" : '',
            ]);

            foreach ($menuItem['childrens'] as $children){
                $title = $children['title']['en'];
                $page = \App\Models\Page::where('title', $title)->first();
                MenuItem::firstOrCreate([
                    'menu_id'    => $menu->id,
                    'title'      => $title,
                    'url'        => '',
                    'target'     => '_self',
                    'icon_class' => null,
                    'color'      => null,
                    'parent_id'  => $parent->id,
                    'order'      => 1,
                    'route'      => !empty($page) ? 'page.one' : mb_strtolower($title),
                    'parameters' => !empty($page) ? "{\"page\" : \"{$page->slug}\"}" : '',
                ]);
            }
        }

        $this->command->info("Seeding finished");
    }

    /**
     * Getting stub file by filename
     * @param string $filename
     * @return mixed
     */
    protected function getStub($filename = 'menu.php')
    {
        $file = resource_path("stubs/" . $filename);

        if (file_exists($file)) {
            return require($file);
        } else {
            echo "Stub file {$file} doesn\'t exist";
        }
    }
}
