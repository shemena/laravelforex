<?php
namespace Database\Seeds\Custom;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;
use Faker\Factory;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $host  = last(explode('//', config('app.url')));

        if (User::count() == 0) {

            $admin             = Role::firstOrCreate([
                'name'         => 'admin',
                'display_name' => 'Administrator',
            ]);
            $content_manager   = Role::firstOrCreate([
                'name'         => 'content',
                'display_name' => 'Content manager',
            ]);
            $affiliate_manager = Role::firstOrCreate([
                'name'         => 'affiliate',
                'display_name' => 'Affiliate manager',
            ]);

            // Creating accounts
            $superAdmin = User::firstOrCreate([
                'role_id'  => $admin->id,
                'name'     => 'SuperAdmin',
                'email'    => 'admin@admin.com',
                'avatar'   => 'users/default.png',
                'password' => bcrypt('password'),
            ]);
            // Affiliate account
            $affiliate = User::firstOrCreate([
                'role_id'  => $affiliate_manager->id,
                'name'     => $faker->firstName(),
                'email'    => 'affiliate@' . $host,
                'avatar'   => 'users/default.png',
                'password' => bcrypt('affiliate@' . $host),
            ]);
            // Content accounts
            $content_1 = User::firstOrCreate([
                'role_id'  => $content_manager->id,
                'name'     => $faker->firstName(),
                'email'    => 'content_1@' . $host,
                'avatar'   => 'users/default.png',
                'password' => bcrypt('content_1@' . $host),
            ]);
            $content_2 = User::firstOrCreate([
                'role_id'  => $content_manager->id,
                'name'     => $faker->firstName(),
                'email'    => 'content_2@' . $host,
                'avatar'   => 'users/default.png',
                'password' => bcrypt('content_1@' . $host),
            ]);
            $content_3 = User::firstOrCreate([
                'role_id'  => $content_manager->id,
                'name'     => $faker->firstName(),
                'email'    => 'content_3@' . $host,
                'avatar'   => 'users/default.png',
                'password' => bcrypt('content_1@' . $host),
            ]);
        }
    }
}