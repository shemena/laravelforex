<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Wiki::class, function (Faker $faker) {

    $author = App\User::first();

    return [
        'term'            => $title = $faker->text(100),
        'slug'            => $faker->slug(5, true),
        'content'         => $description = $faker->paragraph(2, true),
        'author_id'       => !empty($author) ? $author->id : '',
        'status'          => $faker->boolean($chanceOfGettingTrue = 75),
        'seo_title'       => $title,
        'seo_description' => $description,
        'no_index'        => 0,
        'no_follow'       => 0,
    ];
});
