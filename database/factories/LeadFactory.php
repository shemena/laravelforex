<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Lead::class, function (Faker $faker) {
    return [
        'name'      => $faker->firstNameMale(),
        'last_name'       => $faker->lastName(),
        'email'           => $faker->email(),
        'phone'           => $faker->e164PhoneNumber(),
        'country'         => $faker->country(),
        'language'        => $faker->languageCode(),
        'promo'           => $faker->numberBetween(100, 999),
        'in_crm'          => $faker->boolean(75),
        'source'          => 'local',
    ];
});
