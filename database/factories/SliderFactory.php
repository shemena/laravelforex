<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Slider::class, function (Faker $faker) {

    $author = App\User::first();

    return [
        'title'     => $title = $faker->text(100),
        'h1'        => $faker->text(100),
        'h2'        => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'image'     => $faker->imageUrl($width = 640, $height = 480),
        'image_alt' => $title,
        'status'    => $faker->boolean($chanceOfGettingTrue = 75),
        'order'     => $faker->numberBetween(1, 50),
        'button'    => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'link'      => $faker->url,
        'author_id' => !empty($author) ? $author->id : '',
    ];
});
