<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Document::class, function (Faker $faker) {

    $author = App\User::first();

    return [
        'title'           => $title = $faker->text(100),
        'file'            => $faker->imageUrl($width = 640, $height = 480),
        'status'          => $faker->boolean($chanceOfGettingTrue = 75),
        'author_id'       => !empty($author) ? $author->id : '',
        'order'           => $faker->numberBetween(1, 50),
        'page'            => $faker->word(),
    ];
});
