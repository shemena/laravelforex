<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Landing::class, function (Faker $faker) {

    $author = App\User::first();

    return [
        'name'            => $faker->text(30),
        'slug'            => $faker->slug(5, true),
        'view'            => 'default',
        'author_id'       => !empty($author) ? $author->id : '',
        'status'          => $faker->boolean($chanceOfGettingTrue = 75),
        'visits'          => $faker->numberBetween(1, 1000),
    ];
});
