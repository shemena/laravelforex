<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\DemoLead::class, function (Faker $faker)
{
    return [
        'name'     => $faker->firstNameMale(),
        'email'    => $faker->email(),
        'phone'    => $faker->e164PhoneNumber(),
        'country'  => $faker->country(),
        'language' => $faker->languageCode(),
        'source'   => 'local',
    ];
});
