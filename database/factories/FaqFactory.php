<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Faq::class, function (Faker $faker) {

    $author = App\User::first();

    return [
        'question'        => $title = $faker->text(100),
        'slug'            => $faker->slug(5, true),
        'answer'          => $description = $faker->paragraph(2, true),
        'status'          => $faker->boolean($chanceOfGettingTrue = 75),
        'order'           => $faker->numberBetween(1, 100),
        'author_id'       => !empty($author) ? $author->id : '',
        'seo_title'       => $title,
        'seo_description' => $description,
        'no_index'        => 0,
        'no_follow'       => 0,
    ];
});
