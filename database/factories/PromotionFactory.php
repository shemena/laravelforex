<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Promotion::class, function (Faker $faker) {

    $author = App\User::first();

    return [
        'title'           => $title = $faker->text(100),
        'slug'            => $faker->slug(5, true),
        'content'         => $faker->paragraph(5, true),
        'excerpt'         => $description = $faker->paragraph(2, false),
        'image'           => $faker->imageUrl($width = 640, $height = 480),
        'status'          => $faker->boolean($chanceOfGettingTrue = 75),
        'category_id'     => null,
        'author_id'       => !empty($author) ? $author->id : '',
        'seo_title'       => $title,
        'seo_description' => $description,
        'no_index'        => 0,
        'no_follow'       => 0,
    ];

});
