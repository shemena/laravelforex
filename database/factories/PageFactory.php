<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Page::class, function (Faker $faker) {

    $author = App\User::first();

    return [
        'title'           => $title = $faker->text(100),
        'slug'            => $faker->slug(5, true),
        'content'         => $faker->paragraph(2, true),
        //'excerpt'         => $description = $faker->text(160),
        'status'          => $faker->boolean($chanceOfGettingTrue = 75),
        'image'           => $faker->imageUrl($width = 640, $height = 480),
        'image_alt'       => $title,
        'author_id'       => !empty($author) ? $author->id : null,
        'seo_title'       => $title,
        'seo_description' => $faker->text(160),
        'no_index'        => 0,
        'no_follow'       => 0,
    ];
});
