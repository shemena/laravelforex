<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateNewsTable extends Migration
{
    protected static $table       = 'news';
    protected static $model       = \App\Models\News::class;
    protected static $field_rules = [
        'id'              => [
            'type'     => 'number',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '{}',
            'order'    => 1,
        ],
        'title'           => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '{}',
            'order'    => 2,
        ],
        'slug'            => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '{"slugify":{"origin":"title"}}',
            'order'    => 3,
        ],
        'content'         => [
            'type'     => 'rich_text_box',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '{}',
            'order'    => 4,
        ],
        'excerpt'         => [
            'type'     => 'text_area',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '{}',
            'order'    => 5,
        ],
        'image'           => [
            'type'     => 'image',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '{"resize":{"width":"1000","height":"null"},"quality":"70%","upsize":true,"thumbnails":[{"name":"medium","scale":"50%"},{"name":"small","scale":"25%"},{"name":"cropped","crop":{"width":"300","height":"250"}}]}',
            'order'    => 6,
        ],
        'image_alt'       => [
            'type'     => 'text',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '{}',
            'order'    => 7,
        ],
        'category_id'     => [
            'type'     => 'number',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '{}',
            'order'    => 8,
        ],
        'author_id'       => [
            'type'     => 'text_area',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '{}',
            'order'    => 9,
        ],
        'seo_title'       => [
            'type'     => 'text',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '{}',
            'order'    => 10,
        ],
        'seo_description' => [
            'type'     => 'text',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '{}',
            'order'    => 11,
        ],
        'no_index'        => [
            'type'     => 'checkbox',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '{"on":"Yes","off":"No","checked":"No"}',
            'order'    => 12,
        ],
        'no_follow'       => [
            'type'     => 'checkbox',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '{"on":"Yes","off":"No","checked":"No"}',
            'order'    => 13,
        ],
        'created_at'      => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 1,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '{}',
            'order'    => 14,
        ],
        'updated_at'      => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 1,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '{}',
            'order'    => 15,
        ],
    ];

    /**
     * Creating table if not exist
     */
    public function createTable()
    {
        if (!Schema::hasTable(self::$table)) {
            Schema::create(self::$table, function (Blueprint $table) {
                $table->increments('id');
                $table->string('slug')->unique();
                $table->string('title');
                $table->string('excerpt',160)->nullable();
                $table->text('content')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->string('image')->nullable();
                $table->string('image_alt')->nullable();
                $table->unsignedInteger('category_id')->index()->nullable();
                $table->unsignedInteger('author_id')->index();
                $table->string('seo_title')->nullable();
                $table->string('seo_description')->nullable();
                $table->boolean('no_index')->default(0);
                $table->boolean('no_follow')->default(0);
                $table->timestamps();
            });
        }else{
            throw new Exception('Table ' . self::$table . ' already exist.');
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try{
            $this->createTable();
        }catch (Exception $exception){
            $this->down();
            throw $exception;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try{
            Schema::dropIfExists(self::$table);
            self::deleteMigration();
        }catch (Exception $exception){
            throw $exception;
        }
    }

    /**
     * Delete post in migration table about current migration
     */
    public static function deleteMigration()
    {
        $file = last(explode('/', __FILE__));
        $filename = explode('.php', $file);
        DB::table('migrations')->where('migration', $filename)->delete();
    }
}
