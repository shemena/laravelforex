<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManualsTable extends Migration
{
    protected static $table = 'manuals';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable(self::$table)){
            Schema::create(self::$table, function (Blueprint $table) {
                $table->increments('id');
                $table->string('question');
                $table->text('answer');
                $table->string('slug')->unique()->index();
                $table->unsignedInteger('author_id')->index();
                $table->tinyInteger('status')->default(0)->index();
                $table->unsignedInteger('order')->nullable()->index();
                $table->string('seo_title', 160)->nullable();
                $table->text('seo_description')->nullable();
                $table->boolean('no_index')->default(0);
                $table->boolean('no_follow')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
