<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgainPagesTable extends Migration
{
    protected static $table = 'pages';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable(self::$table)){
            Schema::create(self::$table, function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->string('slug')->unique();
                $table->text('content')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->string('image')->nullable();
                $table->string('image_alt')->nullable();
                $table->unsignedInteger('author_id')->index();
                $table->string('seo_title')->nullable();
                $table->string('seo_description')->nullable();
                $table->boolean('no_index')->default(0);
                $table->boolean('no_follow')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
