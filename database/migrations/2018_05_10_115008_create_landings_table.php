<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingsTable extends Migration
{
    protected static $table = 'landings';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable(self::$table)){
            Schema::create(self::$table, function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('slug')->index()->unique();
                $table->string('view');
                $table->string('status')->index();
                $table->unsignedInteger('visits')->index()->default(0);
                $table->unsignedInteger('author_id')->index();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
