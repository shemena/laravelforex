<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWikiTable extends Migration
{
    protected static $table = 'wiki';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::$table)) {
            Schema::create(self::$table, function (Blueprint $table) {
                $table->increments('id');
                $table->string('term');
                $table->text('content')->nullable();
                $table->string('slug')->unique();
                $table->integer('author_id')->unsigned()->index();
                $table->boolean('status')->default(0);
                $table->text('seo_title')->nullable();
                $table->string('seo_description')->nullable();
                $table->boolean('no_index')->default(0);
                $table->boolean('no_follow')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
