<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSliderTable extends Migration
{
    protected static $table = 'sliders';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::$table)) {
            Schema::create(self::$table, function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->string('image');
                $table->string('image_alt')->nullable();
                $table->string('h1')->nullable();
                $table->text('h2')->nullable();
                $table->string('link')->nullable();
                $table->string('button')->nullable();
                $table->unsignedInteger('order')->nullable();
                $table->unsignedInteger('author_id')->index();
                $table->boolean('status')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
