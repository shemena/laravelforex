<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeletePagesTable extends Migration
{
    protected static $table = 'pages';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists(self::$table);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable(self::$table)) {
            Schema::create(self::$table, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('author_id');
                $table->string('title');
                $table->text('excerpt')->nullable();
                $table->text('body')->nullable();
                $table->string('image')->nullable();
                $table->string('slug')->unique();
                $table->text('meta_description')->nullable();
                $table->text('meta_keywords')->nullable();
                $table->enum('status', \TCG\Voyager\Models\Page::$statuses)->default(\TCG\Voyager\Models\Page::STATUS_INACTIVE);
                $table->timestamps();
            });
        }
    }
}
