<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorsTable extends Migration
{
    protected static $table = 'visitors';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::$table)) {
            Schema::create(self::$table, function (Blueprint $table) {
                $table->increments('id');
                $table->string('ip', 100)->index();
                $table->string('uniqid', 30)->unique();
                $table->unsignedInteger('count_visits')->default(0);
                $table->text('browser')->nullable();
                $table->text('referer')->nullable();
                $table->string('device')->nullable();
                $table->string('country')->nullable();
                $table->tinyInteger('in_blacklist')->default(false);
                $table->tinyInteger('in_whitelist')->default(false);
                $table->timestamps();
            });
        } else {
            exit('Table ' . self::$table . ' not found!');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
