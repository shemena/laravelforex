<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration
{
    protected static $table = 'leads';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::$table)) {
            Schema::create(self::$table, function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 20);
                $table->string('last_name', 100);
                $table->string('email', 50);
                $table->string('phone', 30);
                $table->string('country', 50);
                $table->string('language', 2);
                $table->boolean('in_crm')->default(0);
                $table->unsignedInteger('promo')->nullable();
                $table->string('source', 100)->index();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(self::$table);
    }
}
