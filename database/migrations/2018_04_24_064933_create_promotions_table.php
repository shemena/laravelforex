<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    protected static $table = 'promotions';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(self::$table)) {
            Schema::create(self::$table, function (Blueprint $table) {
                $table->increments('id');
                $table->string('slug')->unique()->index();
                $table->string('title');
                $table->text('content')->nullable();
                $table->string('excerpt',160)->nullable();
                $table->string('image')->nullable();
                $table->string('image_alt')->nullable();
                $table->tinyInteger('status')->default(0)->index();
                $table->unsignedInteger('category_id')->nullable()->index();
                $table->unsignedInteger('author_id')->index();
                $table->string('seo_title', 160)->nullable();
                $table->text('seo_description')->nullable();
                $table->boolean('no_index')->default(0);
                $table->boolean('no_follow')->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
