<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'ipdata' => [
        'host'    => 'https://api.ipdata.co/',
        'api_key' => 'd17a7ccfd491889e7400e8778289e8d9fe0b5bb445760a766306c5a2',
    ],

    'crm' => [
        'host'                => 'http://crm.laravel.com/api/v2/',
        'routes'              => [
            'lead' => [
                'create'      => 'lead',
                'check-email' => 'check-email',
                'check-phone' => 'check-phone',
            ],
        ],
        'supported_languages' => [
            'en' => 'English',
            'it' => 'Italian',
            'ru' => 'Russian',
            'fr' => 'French',
        ],
    ],

    'backoffice' => [
        'host' => 'http://secure.laravel.com/',
    ],

    'trackbox' => [
        'host'    => 'https://platform.wetrafficasa.com/api/',
        'api_key' => '2643889w34df345676ssdas323tgc738',
        'auth'    => [
            'username' => 'clicksure',
            'password' => 'clicksure',
        ],
        'routes' => [
            'lead' => [
                'send' => 'signup/procform',
            ],
        ],
    ],
];
