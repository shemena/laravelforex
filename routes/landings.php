<?php

Route::group([
    'domain' => \App\Models\Landing::getDomain(),
    'prefix' => LaravelLocalization::setLocale(),
], function () {

    Route::get('/',function (){
        return redirect(env('APP_URL'));
    });

    /**
     * LandingController routes
     */
    Route::get('/{landing}','LandingController@one')->name('landings.one');
});
