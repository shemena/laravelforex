<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function ()
{
    Voyager::routes();

    Route::get('visits', function ()
    {
        return view('admin.visits', []);
    })->name('admin.visits');
});

Route::group(
    [
        'prefix'     => LaravelLocalization::setLocale(),
        'middleware' => [
            'localize',
            'visits',
        ],
    ],
    function ()
    {

        /**
         * HomeController routes
         */
        Route::get('/', 'HomeController@index')->name('home');

        /**
         * Controller routes
         */
        Route::get('/thank-you', 'Controller@thankYou')->name('thank-you');

        /**
         * WikiController routes
         */
        Route::get('/wiki', 'WikiController@all')->name('wiki');
        Route::get('/wiki/{wiki}', 'WikiController@one')->name('wiki.one');

        /**
         * NewsController routes
         */
        Route::get('/news', 'NewsController@all')->name('news');
        Route::get('/news/{news}', 'NewsController@one')->name('news.one');

        /**
         * FaqController routes
         */
        Route::get('/faq', 'FaqController@all')->name('faq');
        Route::get('/faq/{faq}', 'FaqController@one')->name('faq.one');

        /**
         * ManualController routes
         */
        Route::get('/manuals', 'ManualController@all')->name('manuals');
        Route::get('/manuals/{manual}', 'ManualController@one')->name('manuals.one');

        /**
         * PromotionController routes
         */
        Route::get('/promotions', 'PromotionController@all')->name('promotions');
        Route::get('/promotions/{promotion}', 'PromotionController@one')->name('promotions.one');

        /**
         * LeadController routes
         */
        Route::get('/login', 'LeadController@login')->name('login');
        Route::get('/register', 'LeadController@register')->name('live-register');
        Route::get('/demo-register', 'LeadController@demoRegister')->name('demo-register');

        /**
         * AffiliateController routes
         */
        Route::get('/affiliates', 'AffiliateController@index')->name('affiliates');
        Route::post('/affiliates/create', 'AffiliateController@create')->name('affiliate-create');

        /**
         * PageController routes
         */
        Route::get('/{page}', 'PageController@one')->name('page.one');
    });