@component('layouts.index')
    @slot('title',$faq->getTranslatedAttribute('question').' - '.setting('site.title'))
    @slot('pageCss','page')
    @slot('pageJs','page')

    <main class="site-content mr page-faq__one" data-room="{{$socketRoom}}">
        @include("template.bread",['title' => $faq->getTranslatedAttribute('question'),'titlePar' => trans('titles.faq'),'route' => route('faq'),'parent' => false])
        <div class="box-small gui">
            <h1 id="h1">{{$faq->getTranslatedAttribute('question') }}</h1>
            {!! $faq->getTranslatedAttribute('answer') !!}
        </div>
    </main>

@endcomponent