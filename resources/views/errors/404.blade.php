@component('layouts.index')
    @slot('title','404 - '.setting('site.title'))
    @slot('pageCss','404')
    @slot('pageJs','page')

    <main class="site-content mr page-error" data-room="error">
        <div class="error box">
            <div class="error__text">
                <p class="error__404">404</p>
                <p class="error__oops">{!! trans('errors.404') !!}</p>
                <a class="error__btn" href="{{route('home')}}">@lang('button.home')</a>
            </div>
        </div>
    </main>

@endcomponent