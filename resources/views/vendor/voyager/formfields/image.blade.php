@foreach(config('voyager.multilingual.locales') as $lang)
    <div class="image-lang" data-lang="{{$lang}}" style="{{$lang === config('voyager.multilingual.default') ? 'display: block;' : 'display: none;'}}">
        @if(isset($dataTypeContent->{$row->field}))
            @php($img_path = json_decode(get_field_translations($dataTypeContent, $row->field), true)[$lang])
            @if(!empty($img_path))
            <img src="@if( !filter_var($img_path, FILTER_VALIDATE_URL)){{ Voyager::image( $img_path ) }}@else{{ $img_path }}@endif"
                 style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
            @endif
        @endif

        <input @if($row->required == 1 && !isset($dataTypeContent->{$row->field}) && ($lang === config('voyager.multilingual.default'))) required @endif type="file" name="{{ $row->field }}[{{$lang}}]" accept="image/*">
    </div>
@endforeach
