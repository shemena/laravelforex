@if (is_field_translatable($dataTypeContent, $row))
    @if ($row->type !== 'image')
        <span class="language-label js-language-label"></span>
        <input type="hidden"
               data-i18n="true"
               name="{{ $row->field }}_i18n"
               id="{{ $row->field }}_i18n"
               value="{{ get_field_translations($dataTypeContent, $row->field) }}">
    @else
        <input type="hidden" name="{{ $row->field }}_path" value="{{ json_decode(get_field_translations($dataTypeContent, $row->field), true)[config('voyager.multilingual.default')] }}">
        <span class="language-label js-language-label"></span>
        <input type="hidden"
               data-i18n="true"
               name="{{ $row->field }}_i18n"
               id="{{ $row->field }}_i18n"
               value="{{ get_field_translations($dataTypeContent, $row->field) }}">
    @endif
@endif
