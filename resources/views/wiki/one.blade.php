@component('layouts.index')
    @slot('title',$wiki->getTranslatedAttribute('term').' - '.setting('site.title'))
    @slot('pageCss','page')
    @slot('pageJs','page')

    <main class="site-content mr page-wiki__one" data-room="{{$socketRoom}}">
        @include("template.bread",['title' => $wiki->getTranslatedAttribute('term'),'titlePar' => trans('titles.wiki'),'route' => route('wiki'),'parent' => false])
        <div class="box-small gui">
            <h1 id="h1">{{ !empty($wiki->getTranslatedAttribute('term')) ? $wiki->getTranslatedAttribute('term') : '' }}</h1>
            {!! !empty($wiki->getTranslatedAttribute('content')) ? $wiki->getTranslatedAttribute('content') : '' !!}
        </div>
    </main>

@endcomponent