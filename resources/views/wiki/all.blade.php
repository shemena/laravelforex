@component('layouts.index')
    @slot('title',trans('titles.wiki').' - '.setting('site.title'))
    @slot('pageCss','wiki')
    @slot('pageJs','wiki')

    <main class="site-content mr page-wiki" data-room="{{$socketRoom}}">
        @include("template.bread",['title' => trans('titles.wiki'),'parent' => true])
        <div class="box-small wiki">
            <h1 id="h1" class="wiki__title">@lang('titles.wiki')</h1>
            <div class="wiki__alf">
                <ul class="wiki__alf--list">
                    @if (!empty($wiki))
                        @foreach ($wiki as $letter=>$key)
                            @if(count($key)>0)
                                <li><a class="wiki-to" href="#letter_{{$letter}}">{{$letter}}</a></li>
                            @else
                                <li class="disabled"><span>{{$letter}}</span></li>
                            @endif
                        @endforeach
                    @endif
                </ul>
            </div>
            <div class="wiki__block">
                @if (!empty($wiki))
                    @foreach ($wiki as $letter=>$key)
                        @if(count($key)>0)
                            <div id="letter_{{$letter}}" class="wiki__block--list">
                                <h4 class="wiki__block--letter">{{$letter}}</h4>
                                @foreach($key as $word)
                                    <div class="wiki__block--word">
                                        <a href="{{!empty($word->url) ? $word->url : ''}}" class="wiki__block--h5">{{$word->getTranslatedAttribute('term')}} <span class="wiki__block--trans">{{$word->getTranslatedAttribute('term','en')}}</span></a>
                                        <div class="wiki__block--desc">{!! $word->getTranslatedAttribute('content') !!}</div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </main>

@endcomponent