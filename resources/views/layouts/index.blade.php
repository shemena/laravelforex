<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" itemscope itemtype="https://schema.org/Webpage">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> {{ $title  or setting('site.title')}}</title>

    {{ $meta or ''}}

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <link rel="canonical" href="{{$localizedUrls[$language]}}"/>

    @foreach($localizedUrls as $lang=>$key)
        <link rel="alternate" hreflang="{{$lang}}" href="{{$key}}"/>
    @endforeach

    <meta name="description" content="{{$description or ''}}"/>
    <meta itemprop="description" content="{{$description or ''}}">
    <meta property="og:site_name" content="{{setting('site.title')}}"/>
    <meta property="og:title" content="{{ $title  or setting('site.title')}}"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="{{$localizedUrls[$language]}}"/>
    <meta property="og:locale" content="{{LaravelLocalization::getSupportedLocales()[$language]['regional']}}"/>
    <meta property="og:description" content="{{$description or ''}}"/>
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="{{ $title  or setting('site.title')}}"/>
    <meta name="twitter:description" content="{{$description or ''}}"/>

    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('img/favicon/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('img/favicon/safari-pinned-tab.svg')}}" color="#5bbad5">
    <link rel="shortcut icon" href="{{asset('img/favicon/favicon.ico')}}">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="{{asset('img/favicon/browserconfig.xml')}}">
    <meta name="theme-color" content="#ffffff">
</head>
<body @if(config('app.locale') == 'ar')class="rtl" dir="rtl"@endif>
    <div id="wrapper">
        @if (!isset($acc) && !isset($header))
            @include("components.header.header1")
        @endif

        {{-- dinamic part. define in each folder of page --}}
        {{ $slot }}
        {{-- end dinamic part --}}

        @if (!isset($acc) && !isset($footer))
            @include ("components.footer.footer")
        @endif

    </div> <!-- #wrapper -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset(mix('/css/pages/'.$pageCss.'.css')) }}">

    @if(config('app.locale') == 'ar')
        <link rel="stylesheet" href="{{asset(mix('css/rtl.css'))}}">
    @endif

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @stack('slick')
    <script type="text/javascript" src="{{ asset(mix('/js/pages/'.$pageJs.'.js')) }}"></script>
</body>
</html>