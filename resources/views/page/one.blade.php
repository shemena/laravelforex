@component('layouts.index')
    @slot('title',$page->getTranslatedAttribute('title').' - '.setting('site.title'))
    @slot('pageCss','page')
    @slot('pageJs','page')

    <main class="site-content mr page-page" data-room="{{$socketRoom}}" data-roomAll="page">
        @include("template.bread",['title' => $page->getTranslatedAttribute('title'),'parent' => true])
        <div class="box-small gui">
            <h1 id="h1">{{ !empty($page->getTranslatedAttribute('title')) ? $page->getTranslatedAttribute('title') : '' }}</h1>
            {!! !empty($page->getTranslatedAttribute('content')) ? $page->getTranslatedAttribute('content') : '' !!}
        </div>
    </main>

@endcomponent