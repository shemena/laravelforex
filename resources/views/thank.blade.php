@component('layouts.index')
    @slot('title',trans('thank.title').' - '.setting('site.title'))
    @slot('pageCss','thank')
    @slot('pageJs','page')

    <main class="site-content mr page-thank" data-room="thank">
        <div class="thank box">
            <div class="thank__text">
                <p class="thank__title">@lang('thank.title')</p>
                <p class="thank__subtitle">{!! trans('thank.subtitle') !!}</p>
                <a class="thank__btn" href="{{route('home')}}">{{trans('button.home')}}</a>
            </div>
        </div>
    </main>

@endcomponent