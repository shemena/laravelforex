@component('layouts.index')
    @slot('title',trans('titles.news').' - '.setting('site.title'))
    @slot('pageCss','news')
    @slot('pageJs','page')
    @slot('meta')
        @php($rel_url=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].explode('?',$_SERVER['REQUEST_URI'])[0])
        @if(isset($_GET['page']) && $_GET['page'] == '2')
            <link rel="next" href="{{$rel_url}}?page=3">
            <link rel="prev" href="{{$rel_url}}">
        @elseif(isset($_GET['page']))
            <link rel="next" href="{{$rel_url}}?page={{$_GET['page'] + 1}}">
            <link rel="prev" href="{{$rel_url}}?page={{$_GET['page'] - 1}}">
        @else
            <link rel="next" href="{{$rel_url}}?page=2">
        @endif
    @endslot

    <main class="site-content mr page-news" data-room="{{$socketRoom}}">
        @include("template.bread",['title' => trans('titles.news'),'parent' => true])
        <div class="box-small">
            <div class="news">
                <h1 id="h1" class="news__title">@lang('titles.news')</h1>
                @include('components.news.news',['variable' => $news])
                {{ $news->render('template.pagination') }}
            </div>
        </div>
    </main>

@endcomponent