@component('layouts.index')
    @slot('title',$news->getTranslatedAttribute('title').' - '.setting('site.title'))
    @slot('pageCss','page')
    @slot('pageJs','page')

    <main class="site-content mr page-news__one" data-room="{{$socketRoom}}">
        @include("template.bread",['title' => $news->getTranslatedAttribute('title'),'titlePar' => trans('titles.news'),'route' => route('news'),'parent' => false])
        <div class="box-small">
            <div class="gui">
                <h1 id="h1">{{ !empty($news->getTranslatedAttribute('title')) ? $news->getTranslatedAttribute('title') : '' }}</h1>
                <div class="gui__date">
                    <span>{{ !empty($news->created_at) ? $news->created_at->format('d.m.Y') : '' }}</span>
                </div>
                <img class="gui__img" src="{{$news->image}}" alt="">
                {!! !empty($news->getTranslatedAttribute('content')) ? $news->getTranslatedAttribute('content') : '' !!}
                <div style="clear: both"></div>
            </div>
        </div>
    </main>

@endcomponent