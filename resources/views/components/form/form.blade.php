{{--

Component Form может принимать такие пареметры:
class - class для формы,
route (обязательное) - route для передачи данных
prefix (обязательное) - для того что-бы не повторялись id
button (обязательное) - переводы для кнопки
last-name - поле для фамилии если не нужно значение 'none'

--}}
<form class="form {{$prefix}}__form" action="{{!empty($route) ? $route : ''}}">
    {{ csrf_field() }}
    <div class="form__row">
        <input class="input form__input" id="{{$prefix}}_name" type="text" name="name" required autocomplete="off">
        <label for="{{$prefix}}_name" class="form__label">@lang('form.name')</label>
    </div>
    @if(empty($last_name))
        <div class="form__row">
            <input class="input form__input" id="{{$prefix}}_last_name" type="text" name="last_name" required autocomplete="off">
            <label for="{{$prefix}}_last_name" class="form__label">@lang('form.last_name')</label>
        </div>
    @endif
    <div class="form__row">
        <input class="input form__input" id="{{$prefix}}_email" type="email" name="email" required autocomplete="off">
        <label for="{{$prefix}}_email" class="form__label">@lang('form.email')</label>
    </div>
    <div class="form__row top">
        <div class="form__input select"><span>@lang('form.country')</span></div>
    </div>
    <div class="form__row">
        <input id="{{$prefix}}_phone" class="form__input input" type="tel" name="phone" required autocomplete="off">
        <label for="{{$prefix}}_phone" class="form__label">@lang('form.phone')</label>
    </div>
    @if($prefix != 'demo')
        <div class="form__row">
            <input id="{{$prefix}}_promo" class="form__input" type="text" name="promo" autocomplete="off">
            <label for="{{$prefix}}_promo" class="form__label">@lang('form.promo')</label>
        </div>
    @endif
    <div class="form__row terms">
        <input id="{{$prefix}}_check" type="checkbox" name="check">
        <label for="{{$prefix}}_check" class="form__check">
            <span class="form__terms">
                @lang('form.agree')
                <a href="{{setting('site.terms-conditions')}}">@lang('form.terms')</a>
            </span>
        </label>
    </div>
    <p class="error" data-err="@lang('errors.ajax')" data-field="@lang('errors.field')"></p>
    <button class="form__btn" type="submit">{{!empty($button) ? $button : trans('button.registration')}}</button>
    <input type="hidden" name="source" value="local">
</form>