<header class="header">
    <div class="box header__box">
        <div class="header__burger js-burger">
            <div></div>
        </div>
        @if($pageCss !== 'home')
            <a class="header__logo" href="{{route('home')}}">
                <img src="{{asset('img/logo.png')}}" alt="">
            </a>
        @else
            <div class="header__logo">
                <img src="{{asset('img/logo.png')}}" alt="">
            </div>
        @endif
        <div class="header__right">
            <div class="header__enter">
                <a href="javascript:" class="header__trade">@lang('button.trading')</a>
                <a href="{{route('login')}}" class="header__login">@lang('button.login')</a>
                <a href="{{route('live-register')}}" class="header__register">@lang('button.registration')</a>
            </div>
            <div class="header__icon s-user"></div>
            <div class="header__lang">
                <div class="header__lang--current">
                    <span class="cntr-flag cntr-flag-{{mb_strtolower(last(explode('_', LaravelLocalization::getSupportedLocales()[$language]['regional'])))}}"></span>
                    <span>{{$language}}</span>
                </div>
                <ul class="header__lang--list">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        @if($localeCode != $language && $properties['enable'])
                            <li class="header__lang--item">
                                <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ $localizedUrls[$localeCode] }}">
                                    <span class="cntr-flag cntr-flag-{{ mb_strtolower(last(explode('_', $properties['regional']))) }}"></span>
                                    <span>{{$localeCode}}</span>
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="header__block js-nav">
        <div class="header__block--line">
            <div class="header__block--close"></div>
            <div class="header__lang">
                <ul class="header__lang--list">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        @if($properties['enable'])
                            <li class="header__lang--item @if ($localeCode == $language) active @endif">
                                <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ $localizedUrls[$localeCode] }}">
                                    <span class="cntr-flag cntr-flag-{{ mb_strtolower(last(explode('_', $properties['regional']))) }}"></span>
                                    <span>{{$localeCode}}</span>
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
        {!! menu('header', 'template.header_menu') !!}
        <div class="header__block--footer">
            <div class="header__block--tel"><a href="tel:{{!empty(setting('site.phone-number')) ? setting('site.phone-number') : '+12 345 678 91 01'}}">{{!empty(setting('site.phone-number')) ? setting('site.phone-number') : '+12 345 678 91 01'}}</a></div>
            <div class="header__block--email"><a href="mailto:{{!empty(setting('site.support-email')) ? setting('site.support-email') : 'support@laravel-forex.com'}}">{{!empty(setting('site.support-email')) ? setting('site.support-email') : 'support@laravel-forex.com'}}</a></div>
            <p class="header__block--addr">{{!empty(setting('site.adress')) ? setting('site.adress') : 'Bulgaria, Sofia, Geo Milev District 43, Shipchenski Prohod Blvd., Entrance A, Floor 1, Office 5'}}</p>
        </div>
    </div>
</header>