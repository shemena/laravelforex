<header class="header">
    <div class="box header__box">
        @if($pageCss !== 'home')
            <a class="header__logo" href="{{route('home')}}">
                <img src="{{asset('img/logo.png')}}" alt="">
            </a>
        @else
            <div class="header__logo">
                <img src="{{asset('img/logo.png')}}" alt="">
            </div>
        @endif
        @include('template.header_menu', ['items' => $menu_header])
        <div class="header__right">
            <div class="header__enter">
                <a href="{{route('login')}}" class="header__login">@lang('button.login')</a>
                <a href="{{route('live-register')}}" class="header__register">@lang('button.registration')</a>
            </div>
            <div class="header__icon s-user"></div>
            <div class="header__lang">
                <div class="header__lang--current">
                    <span class="cntr-flag cntr-flag-{{mb_strtolower(last(explode('_', LaravelLocalization::getSupportedLocales()[$language]['regional'])))}}"></span>
                    <span>{{$language}}</span>
                </div>
                <ul class="header__lang--list">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        @if($localeCode != $language && $properties['enable'])
                            <li class="header__lang--item">
                                <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ $localizedUrls[$localeCode] }}">
                                    <span class="cntr-flag cntr-flag-{{ mb_strtolower(last(explode('_', $properties['regional']))) }}"></span>
                                    <span>{{$localeCode}}</span>
                                </a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <div class="header__burger js-burger">
                <div></div>
            </div>
        </div>
    </div>
</header>