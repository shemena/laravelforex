<div class="news__list">
    @if(!$variable->isEmpty())
        @foreach($variable as $item)
            <div class="news__item">
                <div class="news__img" style="background-image: url({{$item->image}});"></div>
                <div class="news__text">
                    <h3 class="news__text--title">{{$item->getTranslatedAttribute('title')}}</h3>
                    <p class="news__text--excerpt">{{$item->getTranslatedAttribute('excerpt')}}</p>
                    <a class="news__text--more" href="{{route('news.one', ['page' => $item->getTranslatedAttribute('slug')])}}">@lang('button.read more')</a>
                </div>
            </div>
        @endforeach
    @else
        <div class="news__item">
            <div class="news__img" style="background-color: grey"></div>
            <div class="news__text">
                <h3 class="news__text--title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, harum.</h3>
                <p class="news__text--excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam doloribus optio quae reprehenderit tempora voluptas!</p>
                <a class="news__text--more" href="{{route('news')}}">@lang('button.read more')</a>
            </div>
        </div>
        <div class="news__item">
            <div class="news__img" style="background-color: grey"></div>
            <div class="news__text">
                <h3 class="news__text--title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse, neque.</h3>
                <p class="news__text--excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est excepturi id illum itaque molestiae?</p>
                <a class="news__text--more" href="{{route('news')}}">@lang('button.read more')</a>
            </div>
        </div>
        <div class="news__item">
            <div class="news__img" style="background-color: grey"></div>
            <div class="news__text">
                <h3 class="news__text--title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, eaque.</h3>
                <p class="news__text--excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus adipisci quas quia repudiandae vel?</p>
                <a class="news__text--more" href="{{route('news')}}">@lang('button.read more')</a>
            </div>
        </div>
        <div class="news__item">
            <div class="news__img" style="background-color: grey"></div>
            <div class="news__text">
                <h3 class="news__text--title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, qui?</h3>
                <p class="news__text--excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi dolor excepturi nostrum quia voluptates?</p>
                <a class="news__text--more" href="{{route('news')}}">@lang('button.read more')</a>
            </div>
        </div>
    @endif
</div>