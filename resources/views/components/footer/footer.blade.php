<footer class="footer">
    <div class="box footer__box">
        <div class="footer__left">
            <img src="{{asset('img/logo-footer.png')}}" alt="Footer Logo" class="footer__logo">
            <div class="footer__contact">
                <div class="footer__tel"><a href="tel:{{!empty(setting('site.phone-number')) ? setting('site.phone-number') : '+12 345 678 91 01'}}">{{!empty(setting('site.phone-number')) ? setting('site.phone-number') : '+12 345 678 91 01'}}</a></div>
                <div class="footer__email"><a href="mailto:{{!empty(setting('site.support-email')) ? setting('site.support-email') : 'support@laravel-forex.com'}}">{{!empty(setting('site.support-email')) ? setting('site.support-email') : 'support@laravel-forex.com'}}</a></div>
                <p class="footer__addr">{{!empty(setting('site.adress')) ? setting('site.adress') : 'Bulgaria, Sofia, Geo Milev District 43, Shipchenski Prohod Blvd., Entrance A, Floor 1, Office 5'}}</p>
            </div>
            @if(!$documents->isEmpty())
                <ul class="footer__doc">
                    @foreach($documents as $document)
                        @if(json_decode($document->file))
                            @foreach(json_decode($document->file) as $file)
                                <li><a target='_blank' href="/storage/{{$file->download_link}}">{{ !empty($document->getTranslatedAttribute('title')) ? $document->getTranslatedAttribute('title') : '' }}</a></li>
                            @endforeach
                        @endif
                    @endforeach
                </ul>
            @else
                <ul class="footer__doc">
                    <li><a href="javascript:">@lang('titles.docs.privacy policy')</a></li>
                    <li><a href="javascript:">@lang('titles.docs.terms and conditions')</a></li>
                    <li><a href="javascript:">@lang('titles.docs.privacy policy')</a></li>
                    <li><a href="javascript:">@lang('titles.docs.anti-money laundering')</a></li>
                </ul>
            @endif
        </div>
        <div class="footer__right">
            <div class="footer__menu">
                @include('template.footer_menu', ['items' => $menu_header])
            </div>
            <div class="footer__terms">
                <h4>@lang('footer.risk')</h4>
                <p>@lang('footer.risk text')</p>
                <h4>@lang('footer.legal')</h4>
                <p>@lang('footer.legal text')</p>
            </div>
            <p class="footer__copy">@lang('footer.copy')</p>
        </div>
    </div>
</footer>