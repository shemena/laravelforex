<div class="advantages">
    <div class="box">
        <h3 class="advantages__title">@lang('advantages.title')</h3>
        @if(!empty(trans('advantages.list')))
            @php($num = count(trans('advantages.list')))
            <div class="advantages__list">
                @foreach(trans('advantages.list') as $key=>$advantage)
                    <div class="advantages__item">
                        <div class="advantages__img advantages-{{$key+1}}"></div>
                        <p class="advantages__text">{{$advantage['text']}}</p>
                        <span class="advantages__num">0{{$key+1}}</span>
                    </div>
                @endforeach
                @if(($num % 3) != 0)
                    <div class="advantages__item last"></div>
                @endif
            </div>
        @endif
    </div>
</div>