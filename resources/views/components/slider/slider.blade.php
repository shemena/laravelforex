{{--

Если для слайдера нет картинок то передаем noBg = true

--}}

<div class="slider">
    @if(!$slider->isEmpty())
        @foreach($slider as $slide)}
            <div class="slider__item" style="{{!empty($noBg) && $noBg ? '' : "background-image: url(".$slide->image.");"}}">
                <div class="box">
                    <h3 class="slider__title">{!! $slide->getTranslatedAttribute('h1') !!}</h3>
                    <p class="slider__text">{!! $slide->getTranslatedAttribute('h2') !!}</p>
                    <a href="{{$slide->getTranslatedAttribute('link')}}"
                       class="slider__link">{{$slide->getTranslatedAttribute('button')}}</a>
                </div>
            </div>
        @endforeach
    @else
        <div class="slider__item">
            <div class="box">
                <h3 class="slider__title">Lorem ipsum dolor sit amet, consectetur adipisicing.</h3>
                <p class="slider__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid mollitia odio recusandae.</p>
                <a href="{{route('live-register')}}"
                   class="slider__link">@lang('button.registration')</a>
            </div>
        </div>
        <div class="slider__item">
            <div class="box">
                <h3 class="slider__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
                <p class="slider__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda expedita fuga magnam velit.</p>
                <a href="{{route('live-register')}}"
                   class="slider__link">@lang('button.registration')</a>
            </div>
        </div>
        <div class="slider__item">
            <div class="box">
                <h3 class="slider__title">Lorem ipsum dolor sit amet, consectetur.</h3>
                <p class="slider__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda expedita facere natus quisquam rem!</p>
                <a href="{{route('live-register')}}"
                   class="slider__link">@lang('button.registration')</a>
            </div>
        </div>
    @endif
</div>

@push('slick')
    <script src="{{asset('js/libs/slick.min.js')}}"></script>
@endpush