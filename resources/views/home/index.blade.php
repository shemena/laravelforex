@component('layouts.index')
    @slot('pageCss','home')
    @slot('pageJs','home')

    <main class="site-content page-home" data-room="{{$socketRoom}}">
        <div class="first">
            @include('components.slider.slider',['noBg'=>true])
            <div class="slider__form">
                <h4 class="slider__form--title">@lang('form.title')</h4>
                <p class="slider__form--subtitle">@lang('form.subtitle')</p>
                @include('components.form.form',['prefix' => 'lead','route' => route('live-register')])
            </div>
        </div>
        <div class="demo">
            <div class="box-small">
                <h3 class="demo__title">@lang('form.title demo')</h3>
                @include('components.form.form',['prefix'=>'demo','class'=>'demo__form','route'=> route('demo-register'),'last_name'=>'none','button'=>trans('button.demo')])
            </div>
        </div>
        @include('components.advantages.advantages')
        <div class="news">
            <div class="box">
                <h3 class="news__title">@lang('titles.news')</h3>
                @include('components.news.news',['variable' => $news])
                <a class="news__btn" href="{{route('news')}}">@lang('button.read all')</a>
            </div>
        </div>
    </main>

@endcomponent