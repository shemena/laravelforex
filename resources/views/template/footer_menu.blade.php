@if(!empty($items))
    @foreach($items as $menu_item)
        <ul>
            @php
                if (!empty($menu_item->url)){
                    if ($language === 'en'){
                        $url_all = $menu_item->url;
                    } else {
                        $url_all = '/'.$language.$menu_item->url;
                    }
                } else {
                    $url_all =  $menu_item->link();
                }
            @endphp
            <li><a href="javascript:">{{ $menu_item->getTranslatedAttribute('title') }}</a></li>
            @if($menu_item->children->count())
                @foreach($menu_item->children as $menu_children)
                    @php
                        if (!empty($menu_children->url)){
                            if ($language === 'en'){
                                $url_all_child = $menu_children->url;
                            } else {
                                $url_all_child = '/'.$language.$menu_children->url;
                            }
                        } else {
                            $url_all_child =  $menu_children->link();
                        }
                    @endphp
                    <li><a href="{{$url_all_child}}">{{$menu_children->getTranslatedAttribute('title')}}</a></li>
                @endforeach
            @endif
        </ul>
    @endforeach
@endif