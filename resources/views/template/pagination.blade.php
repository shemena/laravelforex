<ul class="pagination">
    @if ($paginator->lastPage() > 1)
    <li class="pagination-prev {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
        @if ($paginator->currentPage() == 1)
            <a><span>@lang('button.prev')</span></a>
        @else
            <a href="{{ $paginator->url(1) }}"><span>@lang('button.prev')</span></a>
        @endif
    </li>
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <li class="pagination-a {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
            <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
        </li>
    @endfor
    <li class="pagination-next {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
        @if (($paginator->currentPage() == $paginator->lastPage()))
            <a><span>@lang('button.next')</span></a>
        @else
            <a href="{{ $paginator->url($paginator->currentPage()+1) }}" ><span>@lang('button.next')</span></a>
        @endif
    </li>
    @endif
</ul>
