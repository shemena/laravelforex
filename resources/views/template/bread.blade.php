<div class="box-small">
    <ul class="breadcrumbs" id="breadcrumbs-one" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" href="{{route('home')}}"><span itemprop="name">{{trans('titles.home')}}</span></a>
        </li>
        @if(!$parent)
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="{{$route}}"><span itemprop="name">{{$titlePar}}</span></a>
            </li>
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                <a class="go-to" itemprop="item" href="#h1"><span itemprop="name">{{$title}}</span></a>
            </li>
        @else
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                <a class="go-to" itemprop="item" href="#h1"><span itemprop="name">{{$title}}</span></a>
            </li>
        @endif
    </ul>
</div>