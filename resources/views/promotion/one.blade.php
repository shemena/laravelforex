@component('layouts.index')
    @slot('title',$promotion->getTranslatedAttribute('title').' - '.setting('site.title'))
    @slot('pageCss','page')
    @slot('pageJs','page')

    <main class="site-content mr page-news__one" data-room="{{$socketRoom}}">
        @include("template.bread",['title' => $promotion->getTranslatedAttribute('title'),'titlePar' => trans('titles.promotions'),'route' => route('promotions'),'parent' => false])
        <div class="box-small">
            <div class="gui">
                <h1 id="h1">{{ !empty($promotion->getTranslatedAttribute('title')) ? $promotion->getTranslatedAttribute('title') : '' }}</h1>
                <div class="gui__date">
                    <span>{{ !empty($promotion->create_at) ? $promotion->create_at : '' }}</span>
                </div>
                {!! !empty($promotion->getTranslatedAttribute('content')) ? $promotion->getTranslatedAttribute('content') : '' !!}
            </div>
        </div>
    </main>

@endcomponent