@component('layouts.index')
    @slot('title',trans('titles.manual').' - '.setting('site.title'))
    @slot('pageCss','faq')
    @slot('pageJs','page')

    <main class="site-content mr page-faq" data-room="{{$socketRoom}}">
        @include("template.bread",['title' => trans('titles.manual'),'parent' => true])
        <div class="box-small gui faq">
            <h1 id="h1">@lang('titles.manual')</h1>

            @if (!empty($manual))
                <ol>
                    @foreach ($manual as $item)
                        <li>
                            <a href="{{!empty($item->url) ? $item->url : ''}}">{{!empty($item->getTranslatedAttribute('question')) ? $item->getTranslatedAttribute('question') : ''}}</a>
                        </li>
                    @endforeach
                </ol>
            @endif
        </div>
    </main>

@endcomponent