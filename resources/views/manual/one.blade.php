@component('layouts.index')
    @slot('title',$manual->getTranslatedAttribute('question').' - '.setting('site.title'))
    @slot('pageCss','page')
    @slot('pageJs','page')

    <main class="site-content mr page-faq__one" data-room="{{$socketRoom}}">
        @include("template.bread",['title' => $manual->getTranslatedAttribute('question'),'titlePar' => trans('titles.manual'),'route' => route('manuals'),'parent' => false])
        <div class="box-small gui">
            <h1 id="h1">{{$manual->getTranslatedAttribute('question') }}</h1>
            {!! $manual->getTranslatedAttribute('answer') !!}
        </div>
    </main>

@endcomponent