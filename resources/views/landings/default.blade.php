<!DOCTYPE html>
<html lang="en" itemscope itemtype="http://schema.org/Webpage">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Default</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
</head>
<body>
<div id="wrapper">
    <div class="first box-small">
        <h3 class="first__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. At, laboriosam!</h3>
        <div class="first__block">
            <div class="first__left">
                <h4 class="first__left--title">Lorem ipsum dolor sit amet, consectetur adipisicing.</h4>
                <p class="first__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab animi aperiam
                    debitis delectus dolorum ea, maxime modi odit quisquam reprehenderit.</p>
                <p class="first__text">Aspernatur deleniti deserunt dicta exercitationem fuga libero nobis, tempora
                    velit. Aspernatur ducimus eius itaque nisi non quaerat quas quo quos?</p>
                <p class="first__text">Ab amet aperiam atque, aut, consequatur magni maxime necessitatibus nemo nisi
                    odio perferendis provident quidem recusandae tempora voluptates. Dolorum, minima?</p>
            </div>
            <div class="first__form">
                <h3 class="first__form--title">{{trans('form.title lead')}}</h3>
                <form class="first__form form" action="{{route('live-register')}}">
                    {{ csrf_field() }}
                    <div class="form__row">
                        <input id="name" class="form__input input" type="text" name="name" required autocomplete="off">
                        <label for="name" class="form__label">{{trans('form.name')}}</label>
                    </div>
                    <div class="form__row">
                        <input id="email" class="form__input input" type="email" name="email" required autocomplete="off">
                        <label for="email" class="form__label">{{trans('form.email')}}</label>
                    </div>
                    <div class="form__row top">
                        <div class="form__input select"><span>{{trans('form.country')}}</span></div>
                    </div>
                    <div class="form__row">
                        <input id="phone" class="form__input input" type="tel" name="phone" required autocomplete="off">
                        <label for="phone" class="form__label">{{trans('form.phone')}}</label>
                    </div>
                    <div class="form__row terms">
                        <input id="check" type="checkbox" name="check">
                        <label for="check" class="form__check"></label>
                        <span class="form__terms">{{trans('form.agree')}} <a href="{{setting('main.policy')}}">{{trans('form.terms')}}</a></span>
                    </div>
                    <p class="error" data-err="{{trans('errors.ajax')}}" data-field="{{trans('errors.field')}}"></p>
                    <button class="form__btn" type="submit">{{trans('button.registration')}}</button>
                </form>
            </div>
        </div>
    </div>
    <div class="sec">
        <div class="box-small">
            <h3 class="sec__title">Lorem ipsum dolor sit amet, consectetur adipisicing.</h3>
            <ul class="first__list">
                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima, voluptate.</li>
                <li>Magnam minima odit porro quasi sapiente tempora, unde. Ipsum, vero.</li>
                <li>Commodi consequuntur incidunt ipsum iste maxime porro rerum unde veritatis.</li>
                <li>Ad enim in incidunt ipsa libero magni molestiae sunt ut?</li>
                <li>Accusamus aperiam consectetur dolorum et nobis numquam praesentium reprehenderit sunt.</li>
            </ul>
        </div>
    </div>
    <div class="third box-small">
        <h3 class="third__title">Lorem ipsum dolor sit amet, consectetur adipisicing.</h3>
        <p class="third__text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam minus officiis
            repellat tenetur! Dignissimos harum iure molestiae nemo nisi quasi repellat sint tempore ut! Culpa earum
            neque porro tenetur ut.</p>
        <p class="third__text">Cupiditate, ipsum itaque laudantium quam tempora veritatis! Alias aliquam asperiores
            cumque delectus dolores eaque, labore necessitatibus neque nulla obcaecati provident quis reiciendis
            repudiandae! Harum minus mollitia quaerat! Aut dolorum, porro.</p>
        <p class="third__text">Aspernatur aut blanditiis deserunt dolor doloremque, ducimus eaque eius eveniet hic
            illum laboriosam magnam natus officia perspiciatis praesentium quasi quibusdam quos recusandae
            repellendus repudiandae sed, sint soluta ullam, vitae voluptate?</p>
    </div>
    <div class="four">
        <div class="box-small">
            <div class="four__form">
                <h3 class="four__form--title">{{trans('form.title lead')}}</h3>
                <form class="four__form form" action="{{route('live-register')}}">
                    {{ csrf_field() }}
                    <div class="form__row">
                        <input id="name1" class="form__input input" type="text" name="name" required autocomplete="off">
                        <label for="name1" class="form__label">{{trans('form.name')}}</label>
                    </div>
                    <div class="form__row">
                        <input id="email1" class="form__input input" type="email" name="email" required autocomplete="off">
                        <label for="email1" class="form__label">{{trans('form.email')}}</label>
                    </div>
                    <div class="form__row top">
                        <div class="form__input select"><span>{{trans('form.country')}}</span></div>
                    </div>
                    <div class="form__row">
                        <input id="phone1" class="form__input input" type="tel" name="phone" required autocomplete="off">
                        <label for="phone1" class="form__label">{{trans('form.phone')}}</label>
                    </div>
                    <div class="form__row terms">
                        <input id="check1" type="checkbox" name="check">
                        <label for="check1" class="form__check"></label>
                        <span class="form__terms">{{trans('form.agree')}} <a href="{{setting('main.policy')}}">{{trans('form.terms')}}</a></span>
                    </div>
                    <p class="error" data-err="{{trans('errors.ajax')}}" data-field="{{trans('errors.field')}}"></p>
                    <button class="form__btn" type="submit">{{trans('button.registration')}}</button>
                </form>
            </div>
        </div>
    </div>
    <div class="last box-small">
        <p>{{trans('footer.copy')}}</p>
    </div>
</div> <!-- #wrapper -->

<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ mix('/css/landings/default.css') }}">

<script type="text/javascript">
    function addScript(src) {
        var script = document.createElement('script');
        script.src = src;
        script.async = false;
        document.getElementsByTagName('body')[0].appendChild(script);
    }

    if (!window.jQuery) {
        addScript("https://code.jquery.com/jquery-3.2.1.min.js");
        addScript("{{ mix('/js/landings/default.js') }}");
    } else {
        addScript("{{ mix('/js/landings/default.js') }}");
    }
</script>
</body>
</html>