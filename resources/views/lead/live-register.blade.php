@component('layouts.index')
    @slot('title',trans('form.title lead').' - '.setting('site.title'))
    @slot('acc','')
    @slot('pageCss','lead')
    @slot('pageJs','lead')

    <main class="site-content page-lead" data-room="{{$socketRoom}}">
        <div class="box lead__box">
            <a href="{{route('home')}}" class="lead__form--close"></a>
            <div class="lead__form">
                <h3 class="lead__form--title">@lang('form.title lead')</h3>
                @include('components.form.form',['prefix'=>'live','route' => route('live-register')])
            </div>
        </div>
    </main>

@endcomponent