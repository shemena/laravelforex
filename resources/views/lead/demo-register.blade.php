@component('layouts.index')
    @slot('title',trans('form.title demo').' - '.setting('site.title'))
    @slot('acc','')
    @slot('pageCss','lead')
    @slot('pageJs','lead')

    <main class="site-content page-lead" data-room="{{$socketRoom}}">
        <div class="box lead__box">
            <a href="{{route('home')}}" class="lead__form--close"></a>
            <div class="lead__form">
                <h3 class="lead__form--title">@lang('form.title demo')</h3>
                @include('components.form.form',['prefix'=>'demo','class' => 'demo__form', 'route' => route('demo-register'), 'last_name' => 'none'])
            </div>
        </div>
    </main>

@endcomponent