<?php

return [
    'title'    => 'Thank you for your successful registration!',
    'subtitle' => 'Soon our representatives will contact you.'
];