<?php

return [
    'home'       => 'Home',
    'news'       => 'News',
    'promotions' => 'Promotions',
    'wiki'       => 'Wiki-forex',
    'faq'        => 'FAQ',
    'manual'     => 'Manuals',
    'affiliate'  => 'Affiliate',
    'docs'       => [
        'risk warnings'         => 'Risk warnings',
        'terms and conditions'  => 'Terms and conditions',
        'privacy policy'        => 'Privacy policy',
        'anti-money laundering' => 'Anti-money laundering'
    ]
];