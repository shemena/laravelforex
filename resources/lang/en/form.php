<?php

return [
    'title'           => 'Quick registration',
    'title demo'      => 'Open Demo Account',
    'title lead'      => 'Register to start trading',
    'subtitle'        => 'Fill this fields to start profitable trading',
    'name'            => 'Name',
    'last_name'       => 'Surname',
    'email'           => 'Email',
    'country'         => 'Country',
    'phone'           => 'Phone number',
    'promo'           => 'Promo code',
    'agree'           => 'I agree to the',
    'terms'           => 'Terms of registration',
    'company'         => 'Company name',
    'street'          => 'Street',
    'city'            => 'City',
    'zip'             => 'Postal / Zip-code',
    'website'         => 'Website',
    'type'            => 'Choose your IM Type',
    'im'              => 'IM Account',
    'affiliate'       => [
        'contact'   => 'Contact information',
        'invoice'   => 'Invoice information',
        'website'   => 'Website information',
        'region'    => 'Market information. What are your traffic GEO?',
        'subscribe' => 'I would like to receive the Monthly Payment Statement, Account Information & Affiliate Newsletter by email.',
        'country'   => [
            1  => 'Africa',
            2  => 'Afro Eurasia',
            3  => 'Americas',
            4  => 'Asia',
            5  => 'Australia',
            6  => 'Eurasia',
            7  => 'Europe',
            8  => 'North America',
            9  => 'South America',
            10 => 'United Kingdom'
        ]
    ]
];