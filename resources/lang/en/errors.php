<?php

return [
    '404'   => 'Oooooops... Page not found!<br>The page you are looking for doesn’t exist :(',
    'ajax'  => 'Something went wrong! Please try again later.',
    'email' => 'This email already exists',
    'phone' => 'This phone number already exists',
    'field' => 'Incorrectly filled field'
];