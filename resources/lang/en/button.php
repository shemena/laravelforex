<?php

return [
    'login'        => 'Login',
    'registration' => 'Registration',
    'demo'         => 'Open Demo Account',
    'read more'    => 'Read more',
    'read all'     => 'Read all news',
    'start'        => 'start trading',
    'home'         => 'Go to home',
    'prev'         => 'More recent news',
    'next'         => 'Older news',
    'trading'      => 'Trading account'
];