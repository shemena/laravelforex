<?php

return [
    'risk'       => 'INFORMATION AND HIGH RISK WARNING:',
    'risk text'  => 'Trading with foreign exchange (FX), contract for differences (CFDs) and precious metals carries a high level of risk that may not be suitable for all investors. Leverage creates additional risk and loss exposure. Before you decide to trade foreign exchange or contract for differences, carefully consider your investment objectives, experience level, and risk tolerance. Your total losses can exceed the amount of your initial payment and you will have a full additional payment liability to cover all deficits on your account every time.',
    'legal'      => 'LEGAL DISCLAIMER:',
    'legal text' => 'Forex trading entails significant risks and is not appropriate for all investors. The possibility of incurring substantial losses should be taken into account. It is therefore important to understand the possible consequences of investing. Traders should weigh their earning potential against the risks involved and act accordingly.',
    'copy'       => "© 2018 " . setting('site.title') . " All rights reserved"
];