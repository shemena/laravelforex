<?php

return [
  'visitors' => [
      'title'    => 'Visitors',
      'text'     => 'You have <span>0</span> visitors on your site. Click on button below to view all visitors.',
      'btn text' => 'View all visits'
  ]
];