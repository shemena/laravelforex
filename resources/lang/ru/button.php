<?php

return [
    'login'        => 'Личный кабинет',
    'registration' => 'Регистрация',
    'demo'         => 'Открыть демо-счет',
    'read more'    => 'Узнать больше',
    'read all'     => 'Читать все',
    'start'        => 'Инвестировать',
    'home'         => 'На главную',
    'prev'         => 'Предыдущая',
    'next'         => 'Следущая',
    'trading'      => 'Trading account'
];