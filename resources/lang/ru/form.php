<?php

return [
    'title'           => 'Быстрая регистрация',
    'title demo'      => 'Открыть демо счет',
    'title lead'      => 'Зарегистрируйся, чтобы начать инвестировать',
    'subtitle'        => 'Заполните поля, чтобы начать прибыльную торговлю',
    'name'            => 'Имя',
    'last_name'       => 'Фамилия',
    'email'           => 'E-mail',
    'country'         => 'Страна',
    'phone'           => 'Номер телефона',
    'promo'           => 'Промо-код',
    'agree'           => 'Я согласен с ',
    'terms'           => 'условиями регистрации',
    'company'         => 'Название компании',
    'street'          => 'Улица',
    'city'            => 'Город',
    'zip'             => 'Почтовый индекс',
    'website'         => 'Веб-сайт',
    'type'            => 'Выберите свой тип IM',
    'im'              => 'IM аккаунт',
    'affiliate'       => [
        'contact'   => 'Контактная информация',
        'invoice'   => 'Информация о счете',
        'website'   => 'Информация о веб-сайте',
        'region'    => 'Информация о рынке. Каков ваш GEO трафик?',
        'subscribe' => 'Я хотел бы получить Ежемесячное заявление о платеже, информацию об учетной записи и бюллетень для партнеров по электронной почте.',
        'country'   => [
            1  => 'Африка',
            2  => 'Афро Евразия',
            3  => 'Северные и Южная Америка',
            4  => 'Азия',
            5  => 'Австралия',
            6  => 'Евразия',
            7  => 'Европа',
            8  => 'Северная Америка',
            9  => 'Южная Америка',
            10 => 'Великобритания'
        ]
    ]
];