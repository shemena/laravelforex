<?php

return [
    'home'       => 'Главная',
    'news'       => 'Новости',
    'promotions' => 'Акции',
    'wiki'       => 'Вики-словарь',
    'faq'        => 'F.A.Q.',
    'manual'     => 'Руководство',
    'affiliate'  => 'Партнер',
    'docs'       => [
        'risk warnings'         => 'Уведомление о рисках',
        'terms and conditions'  => 'Политика Конфиденциальности',
        'privacy policy'        => 'Условия и Положения',
        'anti-money laundering' => 'Предотвращение отмывания денег'
    ]
];