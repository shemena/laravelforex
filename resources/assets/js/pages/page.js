'use strict'

require('../components/burger')
require('../components/socket')

import scrollTo from '../modules/go-to'

scrollTo($('.go-to'))
