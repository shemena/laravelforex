'use strict'

import scrollTo from '../modules/go-to'

require('../components/burger')
require('../components/socket')

const wiki = $('.wiki__alf--list li')
const wikiA = $('.wiki__alf--list li a')
let wikiSc = $('.wiki__alf').offset().top

wiki.on('click', function () {
  let $this = $(this)
  wiki.removeClass('active')
  $this.addClass('active')
})

wikiA.on('click', function () {
  let $this = $(this)
  wiki.removeClass('active')
  $this.parent().addClass('active')
})

$(window).on('scroll', function () {
  let scroll = $(this).scrollTop()

  if (scroll > 2 * wikiSc) {
    $('.wiki__alf').addClass('fixed')
  } else {
    $('.wiki__alf').removeClass('fixed')
  }
})

scrollTo($('.wiki-to'), (-3.5 * $('.wiki__alf').height()))
