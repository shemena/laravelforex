'use strict'

require('../components/burger')
require('../components/form')
require('../components/socket')

$('.slider').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  speed: 1000,
  dots: false,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 3000
})
