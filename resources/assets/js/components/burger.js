let burger = '.js-burger'
let nav = '.js-nav'
let elements = burger + ', ' + nav
let icon = $('.header__icon')
let enter = $('.header__enter')

$(burger).on('click', function (e) {
  e.preventDefault(e)
  $(burger).hasClass('js-active') ? $(elements).removeClass('js-active') : $(elements).addClass('js-active')
})

$('.header__block--close').on('click', function () {
  $(elements).removeClass('js-active')
})

$(document).mouseup(function (e) { // событие клика по веб-документу
  let div = $(nav) // тут указываем ID элемента
  if (!div.is(e.target) && // если клик был не по нашему блоку
     div.has(e.target).length === 0 && !$(burger).is(e.target)) { // и не по его дочерним элементам
    $(elements).removeClass('js-active') // скрываем его
  }

  if (!icon.is(e.target) && !enter.is(e.target) && enter.has(e.target).length === 0) {
    icon.removeClass('active')
    enter.removeClass('active')
  }
})

icon.on('click', function () {
  $(this).toggleClass('active')
  enter.toggleClass('active')
})