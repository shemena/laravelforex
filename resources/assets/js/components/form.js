import Validate, {PATTERN} from '../modules/validate'

import '@koterion/country_list'

let inputForCheck = $('.input')

inputForCheck.map(function () {
  if (this.value.length > 0) {
    Validate.input(PATTERN[this.name], this)
  }
})

let form = $('.form')
let promo = $('input[name=promo]')
let select = $('.select')

if (select.length > 0) {
  select.map(function () {
    countryList($(this), {
      select: true,
      geo: {
        url: 'https://api.ipdata.co/?api-key=2b5c738b53337ca923ca44e9b9fa107cd90da2cf92283e2186606732',
        getIso: function (response) {
          return response.country_code
        }
      }
    })
  })
}

inputForCheck.on('change', function () {
  Validate.input(PATTERN[this.name], this)
})

promo.on('change', function () {
  Validate.input(PATTERN[this.name], this)
})

form.on('submit', function (event) {
  event.preventDefault()

  let that = $(this)
  let data = that.serialize()
  let url = that.attr('action')
  let btn = that.find('button')
  let curTextBtn = btn.text()
  let errorMessage = that.find('p.error')
  let input = that.find('.input')
  let promoInput = that.find('input[name=promo]')
  let country = that.find('input[name=country]')
  let checkInp = that.find('input[name="check"]')
  let valid = true

  if (!checkInp.prop('checked')) {
    checkInp.addClass('error')
    valid = false
  } else {
    checkInp.removeClass('error')
  }

  input.map(function () {
    let prop = Validate.input(PATTERN[this.name], this)
    if (!prop) {
      valid = false
    }
  })

  if (promoInput.val() !== undefined && promoInput.val().length > 0) {
    let prop = Validate.input(PATTERN[promoInput[0].name], promoInput[0])
    if (!prop) {
      valid = false
    }
  } else {
    data = data.replace('&promo=', '')
  }

  if (country.val() !== undefined && country.val().length > 0) {
    country.prevAll('.select').removeClass('error').addClass('valid')
  } else {
    country.prevAll('.select').addClass('error').removeClass('valid')
    valid = false
  }

  if (valid) {
    btn.text('Wait').attr('disabled', 'disabled')
    errorMessage.text('')

    $.ajax({
      method: 'POST',
      url: url,
      data: data,
      dataType: 'json',
      success: function (request) {
        if (request['success']) {
          location.replace(request['redirect'])
        } else {
          errorMessage.text(errorMessage.data('err'))
        }
        btn.text(curTextBtn)
      },
      error: function (error) {
        let errors = error.responseJSON.errors

        if (errors !== undefined) {
          let emailError = errors.email !== undefined ? errors.email : undefined
          let phoneError = errors.phone !== undefined ? errors.phone : undefined
          let crmError = errors.crm !== undefined ? errors.crm : undefined

          if (emailError !== undefined && phoneError !== undefined) {
            errorMessage.text(phoneError + ' ' + emailError)
          } else if (phoneError !== undefined) {
            errorMessage.text(phoneError)
          } else if (emailError !== undefined) {
            errorMessage.text(emailError)
          } else if (crmError !== undefined) {
            errorMessage.text(crmError)
          } else {
            errorMessage.text(errorMessage.data('err'))
          }
        } else {
          errorMessage.text(errorMessage.data('err'))
        }

        btn.text(curTextBtn).removeAttr('disabled', 'disabled')
      }
    })
  } else {
    errorMessage.text(errorMessage.data('field'))
  }
})
