#!/usr/bin/env php
<?php

function delete($path)
{
    if (is_dir($path) === true){
        $files = array_diff(scandir($path), array('.', '..'));
        foreach ($files as $file)
        {
            delete(realpath($path) . '/' . $file);
        }
        return rmdir($path);
    }
    elseif (is_file($path) === true){
        return unlink($path);
    }
    return false;
}

// ======================================================================================
//                               Стягивание шаблона с репозитория
// ======================================================================================
$branch = 'master';
exec('git init -q');
exec('git remote add origin git@bitbucket.org:shemena/laravelforex.git');
exec('git fetch -q');
exec("git checkout -q {$branch}");
exec('git reset --soft a1c66d8');
exec("git remote rm origin");
echo "Template of the project setup completed!" . PHP_EOL;

// ======================================================================================


// ======================================================================================
//                                    Настройка файла конфига
// ======================================================================================
while (!$db_name)
{
    echo "Enter your database name: ";
    $db_name = fgets(STDIN);
}
while (!$user_name)
{
    echo "Enter your database user name: ";
    $user_name = fgets(STDIN);
}
while (!$password)
{
    echo "Enter your database password: ";
    $password = fgets(STDIN);
}
while (!$app_name)
{
    echo "Enter name of project: ";
    $app_name = fgets(STDIN);
}
while (!$app_url)
{
    echo "Enter domain of project (ex. http(s)://domain.com): ";
    $app_url = fgets(STDIN);
}
$file = file_get_contents('.env.example');
$file = str_replace('DB_DATABASE=', 'DB_DATABASE=' . $db_name, $file);
$file = str_replace('DB_USERNAME=', 'DB_USERNAME=' . $user_name, $file);
$file = str_replace('DB_PASSWORD=', 'DB_PASSWORD=' . $password, $file);
$file = str_replace('APP_NAME=Laravel', 'APP_NAME=' . $app_name, $file);
$file = str_replace('APP_URL=', 'APP_URL=' . $app_url, $file);
file_put_contents('.env', $file);
// ======================================================================================

// ======================================================================================
//                       Доустановка недостающих компонентов шаблона
// ======================================================================================
exec('npm i');
exec('npm run prod');
exec('composer install');
exec('php artisan app:setup');
// ======================================================================================

// ====================================================================================== //
//                   Удаление файлов связанных с развертыванием проекта
// ====================================================================================== //

unlink('setup');
delete('/cli');
delete('/app/Console/Install');

// ======================================================================================

// ====================================================================================== //
//                             Привязка к репозиторию проекта
// ====================================================================================== //
// Получение ссылки на пустой репозиторий                                                 //
//                                      ВНИМАНИЕ!!!                                       //
// (в том случае, если репозиторий не пуст - произойдет затирание ветки "master")         //
// Подключение к удаленном репозиторию и установка в качестве основного                   //
// Добавление всех файлов в индекс и создание коммита (релиз первой версии)               //
// Выталкивание коммита на удаленный репозиторий                                          //
// ====================================================================================== //
while (!$git_repo)
{
    echo PHP_EOL . "Enter url of empty repository (ex. git@bitbucket.org:gmarkg_dev_team/brand_name.git): ";
    $git_repo = fgets(STDIN);
}
if($branch !== 'master'){
    exec("git branch -m {$branch} master");
}
exec("git remote add origin {$git_repo}");
exec('git commit --amend -m "Release v.1.0.0 - Create empty template project"');
exec('git push -f --set-upstream origin master');
// ====================================================================================== //
echo "Success!";