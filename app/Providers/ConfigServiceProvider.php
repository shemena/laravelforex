<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class ConfigServiceProvider extends ServiceProvider
{
    protected static $table = 'localizations';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $localizations = $this->getLocalizationOptions();
        config([
            'laravellocalization.supportedLocales' => $localizations,
            'voyager.multilingual.locales'         => array_keys($localizations),
        ]);
    }

    public function getLocalizationOptions()
    {
        if ((Schema::hasTable(self::$table)) && ((DB::table(self::$table)->count())>0)) {
                $localizations = DB::table(self::$table)->select('*')->where('status', 1)->get();
                $all = [];

                foreach ($localizations as $localization) {
                    $all[$localization->iso_code] = [
                        'name' => $localization->name,
                        'script' => $localization->script,
                        'native' => $localization->native,
                        'regional' => $localization->regional,
                        'enable' => $localization->enable,
                        'alphabet' => json_decode($localization->alphabet, true)
                    ];
                }
                return $all;
        } else {
            return $this->ifTableAbsent();
        }
    }


    protected function ifTableAbsent()
    {
        $all = [];

        foreach ($this->getLocalizationsFile() as $localization) {
            $all[$localization['iso_code']] = [
                'name'     => $localization['name'],
                'script'   => $localization['script'],
                'native'   => $localization['native'],
                'regional' => $localization['regional'],
                'enable'   => $localization['enable'],
                'alphabet' => json_decode($localization['alphabet'], true)
            ];
        }

        return $all;
    }

    protected function getLocalizationsFile($filename = 'localizations.php')
    {
        $file = resource_path("stubs/" . $filename);
        if (file_exists($file)) {
            return require($file);
        } else {
            echo "Stub file {$file} doesn\'t exist";
            exit();
        }
    }
}