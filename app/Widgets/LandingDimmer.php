<?php

namespace App\Widgets;

use App\Models\Landing;
use Illuminate\Support\Str;
use TCG\Voyager\Widgets\BaseDimmer;

class LandingDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count  = Landing::count();
        $string = trans_choice('voyager.dimmer.landing', $count);

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-file-text',
            'title'  => "{$count} {$string}",
            'text'   => __('voyager.dimmer.landing_text', [
                'count'  => $count,
                'string' => Str::lower($string),
            ]),
            'button' => [
                'text' => __('voyager.dimmer.landing_link_text'),
                'link' => route('voyager.landings.index'),
            ],
            'image'  => voyager_asset('images/widget-backgrounds/03.jpg'),
        ]));
    }
}
