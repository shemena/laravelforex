<?php

namespace App\Widgets;

use TCG\Voyager\Widgets\BaseDimmer;

class VisitsDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widget.visits', [
            'icon'   => 'voyager-people',
            'title'  => __('widgets.visitors.title'),
            'text'   => __('widgets.visitors.text'),
            'button' => [
                'text' => __('widgets.visitors.btn text'),
                'link' => route('admin.visits'),
            ],
            'image'  => voyager_asset('images/widget-backgrounds/03.jpg')
        ]);
    }
}
