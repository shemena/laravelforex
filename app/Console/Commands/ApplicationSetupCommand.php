<?php

namespace App\Console\Commands;

use App\Models\Localization;
use Database\Seeds\Custom\AdminUsersSeeder;
use Illuminate\Console\Command;
use App\Models\DemoLead;
use App\Models\Document;
use App\Models\Faq;
use App\Models\Landing;
use App\Models\Lead;
use App\Models\Manual;
use App\Models\News;
use App\Models\Page;
use App\Models\Promotion;
use App\Models\Slider;
use App\Models\Wiki;

class ApplicationSetupCommand extends Command
{
    protected static $models = [
        News::class,
        Promotion::class,
        Wiki::class,
        Faq::class,
        Manual::class,
        Page::class,
        Slider::class,
        Document::class,
        Lead::class,
        DemoLead::class,
        Landing::class,
        Localization::class
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Application setup cli command.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Generate key for application
        $this->call('key:generate');

        // Voyager install
        $this->call('voyager:install');

        try{

            // Call custom seeds
            $this->call('db:seed');

            // Install entities
            foreach (self::$models as $model){
                $this->call('install:' . mb_strtolower(last(explode('\\', $model))));
            }

            // Update permissions
            $this->call('db:seed', [
                '--class' => AdminUsersSeeder::class,
            ]);

        }catch (\Exception $exception){
            $this->info($exception->getMessage());
        }
    }
}
