<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class EntityCreatingCommand extends Command
{
    protected $entity;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:entity {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating different classes for entity usage.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->entity = $this->argument('name');
        $this->create('model');
        $this->create('controller');
        $this->create('migration');
        $this->create('factory');
        $this->create('test');
    }

    /**
     * @param $class
     */
    protected function create($class)
    {
        if (empty($this->entity)){
            return;
        }

        switch ($class){
            case $class === 'model':
                $this->callSilent('make:model', [
                    'name' => config('voyager.models.namespace') . '\\' . $this->entity,
                ]);
                break;
            case $class === 'controller':
                $this->callSilent('make:controller', [
                    'name' => $this->entity . 'Controller',
                ]);
                break;
            case $class === 'migration':
                $this->callSilent('make:migration', [
                    'name' => 'create' . $this->entity . 's_table',
                ]);
                break;
            case $class === 'factory':
                $this->callSilent('make:factory', [
                    'name' => $this->entity . 'Factory',
                ]);
                break;
            case $class === 'test':
                $this->callSilent('make:test', [
                    'name' => $this->entity . 'ControllerTest',
                ]);
                break;
            default:
                echo 'Choose what class you need to create - ex. "controller" ';
        }
    }
}
