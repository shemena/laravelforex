<?php
namespace App\Console\Commands\Install;

use App\Console\Commands\Install\Core\HasInstallAndRevertMethods;
use App\Console\Commands\Install\Core\MustInstall;
use App\Console\Commands\Install\Core\MustRevert;
use App\Models\Menu;
use App\Models\MenuItem;
use App\Models\Wiki;
use Illuminate\Console\Command;

class WikiInstallCommand extends Command implements MustInstall, MustRevert
{
    use HasInstallAndRevertMethods;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:wiki {--revert}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use this command only if table exist in database';

    protected static $table       = 'wiki';
    protected static $model       = Wiki::class;
    protected static $field_rules = [
        'id'              => [
            'type'     => 'number',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 1,
        ],
        'term'            => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 2,
        ],
        'slug'            => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => [
                'slugify' => [
                    'origin' => 'title',
                ],
            ],
            'order'    => 3,
        ],
        'content'         => [
            'type'     => 'rich_text_box',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 4,
        ],
        'author_id'       => [
            'type'     => 'text_area',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 5,
        ],
        'seo_title'       => [
            'type'     => 'text',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 6,
        ],
        'seo_description' => [
            'type'     => 'text',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 7,
        ],
        'no_index'        => [
            'type'     => 'checkbox',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => [
                'on'  => 'Yes',
                'off' => 'No',
            ],
            'order'    => 8,
        ],
        'no_follow'       => [
            'type'     => 'checkbox',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => [
                'on'  => 'Yes',
                'off' => 'No',
            ],
            'order'    => 9,
        ],
        'created_at'      => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 1,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 10,
        ],
        'updated_at'      => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 1,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 11,
        ],
    ];

    /**
     *
     */
    public function createMenuItem()
    {
        $admin_menu = Menu::findOrFail(1);

        MenuItem::firstOrCreate([
            'menu_id'    => $admin_menu->id,
            'title'      => 'Wiki',
            'url'        => '/admin/wiki',
            'target'     => '_self',
            'icon_class' => 'voyager-book',
            'color'      => '#000000',
            'parent_id'  => null,
            'order'      => 8,
            'route'      => null,
            'parameters' => null,
        ]);
    }

    /**
     * @throws \Exception
     */
    public function deleteMenuItem()
    {
        MenuItem::where([
            'title'      => 'Wiki',
            'url'        => '/admin/wiki',
        ])->delete();
    }
}