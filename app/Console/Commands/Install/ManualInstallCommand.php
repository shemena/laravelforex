<?php

namespace App\Console\Commands\Install;

use App\Console\Commands\Install\Core\HasInstallAndRevertMethods;
use App\Console\Commands\Install\Core\MustInstall;
use App\Console\Commands\Install\Core\MustRevert;
use App\Models\Faq;
use App\Models\Manual;
use App\Models\Menu;
use App\Models\MenuItem;
use Illuminate\Console\Command;

class ManualInstallCommand extends Command implements MustInstall, MustRevert
{
    use HasInstallAndRevertMethods;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:manuals {--revert}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use this command only if table exist in database';

    protected static $table       = 'manuals';
    protected static $model       = Manual::class;
    protected static $field_rules = [
        'id'              => [
            'type'     => 'number',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 1,
        ],
        'question'        => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 2,
        ],
        'slug'            => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => [
                'slugify' => [
                    'origin' => 'question',
                ],
            ],
            'order'    => 3,
        ],
        'answer'          => [
            'type'     => 'rich_text_box',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 4,
        ],
        'status'          => [
            'type'     => 'checkbox',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 0,
            'details'  => [
                'on'  => 'Enable',
                'off' => 'Disable',
            ],
            'order'    => 5,
        ],
        'author_id'       => [
            'type'     => 'text_area',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 6,
        ],
        'seo_title'       => [
            'type'     => 'text',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 7,
        ],
        'seo_description' => [
            'type'     => 'text',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 8,
        ],
        'no_index'        => [
            'type'     => 'checkbox',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => [
                'on'  => 'Yes',
                'off' => 'No',
            ],
            'order'    => 9,
        ],
        'no_follow'       => [
            'type'     => 'checkbox',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => [
                'on'  => 'Yes',
                'off' => 'No',
            ],
            'order'    => 10,
        ],
        'created_at'      => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 1,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 11,
        ],
        'updated_at'      => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 1,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 12,
        ],
    ];
    protected        $icon        = 'voyager-logbook';

    /**
     *
     */
    public function createMenuItem()
    {
        $admin_menu = Menu::findOrFail(1);

        MenuItem::firstOrCreate([
            'menu_id'    => $admin_menu->id,
            'title'      => 'Manuals',
            'url'        => '/admin/manuals',
            'target'     => '_self',
            'icon_class' => 'voyager-logbook',
            'color'      => '#000000',
            'parent_id'  => null,
            'order'      => 8,
            'route'      => null,
            'parameters' => null,
        ]);
    }

    /**
     * @throws \Exception
     */
    public function deleteMenuItem()
    {
        MenuItem::where([
            'title' => 'Manuals',
            'url'   => '/admin/manuals',
        ])->delete();
    }
}