<?php
namespace App\Console\Commands\Install;

use App\Console\Commands\Install\Core\HasInstallAndRevertMethods;
use App\Console\Commands\Install\Core\MustInstall;
use App\Console\Commands\Install\Core\MustRevert;
use App\Models\Menu;
use App\Models\MenuItem;
use App\Models\Slider;
use Illuminate\Console\Command;

class SliderInstallCommand extends Command implements MustInstall, MustRevert
{
    use HasInstallAndRevertMethods;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:sliders {--revert}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use this command only if table exist in database';

    protected static $table       = 'sliders';
    protected static $model       = Slider::class;
    protected static $field_rules = [
        'id'              => [
            'type'     => 'number',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 1,
        ],
        'title'           => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 2,
        ],
        'image'           => [
            'type'     => 'image',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => [
                'quality'    => '70%',
                'upsize'     => true,
                'thumbnails' => [
                    [
                        'name'  => 'medium',
                        'scale' => '50%',
                    ],
                    [
                        'name'  => 'small',
                        'scale' => '25%',
                    ],
                    [
                        'name' => 'cropped',
                        'crop' => [
                            'width'  => '300',
                            'height' => '250',
                        ],
                    ],
                ],
            ],
            'order'    => 3,
        ],
        'image_alt'       => [
            'type'     => 'text',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 4,
        ],
        'h1'              => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 5,
        ],
        'h2'              => [
            'type'     => 'text',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 6,
        ],
        'button'          => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 7,
        ],
        'link'            => [
            'type'     => 'text',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 8,
        ],
        'order'           => [
            'type'     => 'number',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 9,
        ],
        'author_id'       => [
            'type'     => 'text_area',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 10,
        ],
        'status'          => [
            'type'     => 'checkbox',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => [
                'on'  => 'Enable',
                'off' => 'Disable',
            ],
            'order'    => 11,
        ],
        'created_at'      => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 1,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 12,
        ],
        'updated_at'      => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 1,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 13,
        ],
    ];

    /**
     *
     */
    public function createMenuItem()
    {
        $admin_menu = Menu::findOrFail(1);

        MenuItem::firstOrCreate([
            'menu_id'    => $admin_menu->id,
            'title'      => 'Sliders',
            'url'        => '/admin/sliders',
            'target'     => '_self',
            'icon_class' => 'voyager-images',
            'color'      => '#000000',
            'parent_id'  => null,
            'order'      => 9,
            'route'      => null,
            'parameters' => null,
        ]);
    }

    /**
     * @throws \Exception
     */
    public function deleteMenuItem()
    {
        MenuItem::where([
            'title'      => 'Sliders',
            'url'        => '/admin/sliders',
        ])->delete();
    }
}