<?php
namespace App\Console\Commands\Install;

use App\Console\Commands\Install\Core\HasInstallAndRevertMethods;
use App\Console\Commands\Install\Core\MustInstall;
use App\Console\Commands\Install\Core\MustRevert;
use App\Models\DemoLead;
use App\Models\Menu;
use App\Models\MenuItem;
use Illuminate\Console\Command;

class DemoLeadInstallCommand extends Command implements MustInstall, MustRevert
{
    use HasInstallAndRevertMethods;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:demoleads {--revert}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use this command only if table exist in database';

    protected static $table             = 'demo_leads';
    protected static $model             = DemoLead::class;
    protected        $icon              = 'voyager-people';
    protected        $displayNamePlural = 'Demo Leads';
    protected static $field_rules       = [
        'id'         => [
            'type'     => 'number',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 1,
        ],
        'name'       => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 2,
        ],
        'email'      => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 3,
        ],
        'phone'      => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 0,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 1,
            'details'  => '',
            'order'    => 4,
        ],
        'country'    => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 5,
        ],
        'language'   => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 6,
        ],
        'source'     => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 7,
        ],
        'created_at' => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 0,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 8,
        ],
        'updated_at' => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 0,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 9,
        ],
    ];

    /**
     *
     */
    public function createMenuItem()
    {
        $admin_menu = Menu::findOrFail(1);
        $leads_menu = \App\Models\MenuItem::firstOrCreate([
            'menu_id'    => $admin_menu->id,
            'title'      => 'Leads',
            'url'        => '',
            'target'     => '_self',
            'icon_class' => 'voyager-people',
            'color'      => '#000000',
            'parent_id'  => null,
            'order'      => 2,
            'route'      => null,
            'parameters' => null,
        ]);

        MenuItem::firstOrCreate([
            'menu_id'    => $admin_menu->id,
            'title'      => 'Demo',
            'url'        => '/admin/demo-leads',
            'target'     => '_self',
            'icon_class' => 'voyager-person',
            'color'      => '#000000',
            'parent_id'  => $leads_menu->id,
            'order'      => 1,
            'route'      => null,
            'parameters' => null,
        ]);
    }

    /**
     * @throws \Exception
     */
    public function deleteMenuItem()
    {
        MenuItem::where([
            'title'      => 'Demo',
            'url'        => '/admin/demo-leads',
        ])->delete();
    }
}