<?php

namespace App\Console\Commands\Install\Core;

use TCG\Voyager\Models\DataType;

interface MustRevert
{
    public function deleteBread(DataType $dataType);

    public function deletePermissions();

    public function removeRowsSettings();

    public function deleteMenuItem();
}