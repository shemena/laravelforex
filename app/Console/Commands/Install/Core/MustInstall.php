<?php
namespace App\Console\Commands\Install\Core;

interface MustInstall
{
    public function createBread();

    public function createPermissions();

    public function setRowsSettings();

    public function createMenuItem();
}