<?php
namespace App\Console\Commands\Install\Core;

use Illuminate\Support\Facades\DB;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;

trait HasInstallMethods
{
    public function install()
    {
        try{
            DB::beginTransaction();
            $this->createBread();
            $this->createPermissions();
            $this->setRowsSettings();
            $this->createMenuItem();
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw $exception;
        }
    }

    public function createBread()
    {
        $this->data_type = DataType::where([
            'name' => static::$table,
            'slug' => str_replace('_', '-', static::$table),
        ])->first();

        if (!is_null($this->data_type)) {
            $this->data_type->update([
                'model_name' => static::$model,
            ]);
        }else{
            $this->data_type = DataType::create([
                'name'                  => static::$table,
                'slug'                  => str_replace('_', '-', static::$table),
                'display_name_singular' => ucfirst(last(explode('\\', static::$model))),
                'display_name_plural'   => property_exists($this, 'displayNamePlural') ? $this->displayNamePlural : ucfirst(static::$table),
                'model_name'            => static::$model,
                'policy_name'           => null,
                'controller'            => null,
                'description'           => null,
                'icon'                  => property_exists($this, 'icon') ? $this->icon : '',
                'generate_permissions'  => 1,
                'server_side'           => 0,
            ]);
        }
    }

    public function createPermissions()
    {
        Permission::generateFor(static::$table);
    }

    public function setRowsSettings()
    {
        foreach (static::$field_rules as $field => $rules) {
            DataRow::firstOrCreate(array_merge(static::$field_rules[$field], [
                'data_type_id' => $this->data_type->id,
                'display_name' => ucfirst($field),
                'field' => $field,
            ]));
        }
    }
}