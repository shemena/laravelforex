<?php
namespace App\Console\Commands\Install\Core;

use Illuminate\Support\Facades\DB;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Permission;

trait HasRevertMethods
{
    /**
     *
     */
    public function revert()
    {
        try{
            DB::beginTransaction();
                $this->deleteMenuItem();
                $this->deletePermissions();
                $this->deleteBread($this->removeRowsSettings());
            DB::commit();
        }catch (\Exception $exception){
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param DataType $dataType
     */
    public function deleteBread(DataType $dataType)
    {
        $dataType->delete();
    }

    /**
     *
     */
    public function deletePermissions()
    {
        Permission::where('table_name', static::$table)->delete();
    }

    /**
     * @return DataType
     * @throws \Exception
     */
    public function removeRowsSettings() : DataType
    {
        $data_type = DataType::where([
            'name' => self::$table,
            'slug' => self::$table,
        ])->firstOrFail();

        DataRow::where([
            'data_type_id' => $data_type->id,
        ])->delete();

        return $data_type;
    }
}