<?php
namespace App\Console\Commands\Install\Core;

trait HasInstallAndRevertMethods
{
    use HasInstallMethods;
    use HasRevertMethods;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @throws \Exception
     */
    public function handle()
    {
        if($this->option('revert')){
            $this->revert();
            $this->info('Reverting process for ' . static::$model . ' complete.');
        }else{
            $this->install();
            $this->info('Installing process for ' . static::$model . ' complete.');
        }
    }
}