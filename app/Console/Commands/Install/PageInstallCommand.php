<?php
namespace App\Console\Commands\Install;

use App\Console\Commands\Install\Core\HasInstallAndRevertMethods;
use App\Console\Commands\Install\Core\MustInstall;
use App\Console\Commands\Install\Core\MustRevert;
use App\Models\Menu;
use App\Models\MenuItem;
use App\Models\Page;
use Illuminate\Console\Command;

class PageInstallCommand extends Command implements MustInstall, MustRevert
{
    use HasInstallAndRevertMethods;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:pages {--revert}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use this command only if table exist in database';

    protected static $table       = 'pages';
    protected static $model       = Page::class;
    protected static $field_rules = [
        'id'              => [
            'type'     => 'number',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 1,
        ],
        'title'           => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 2,
        ],
        'slug'            => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => [
                'slugify' => [
                    'origin' => 'title',
                ],
            ],
            'order'    => 3,
        ],
        'content'         => [
            'type'     => 'rich_text_box',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 4,
        ],
        'status'          => [
            'type'     => 'checkbox',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => [
                'on' => 'Enable',
                'off' => 'Disable',
            ],
            'order'    => 5,
        ],
        'image'           => [
            'type'     => 'image',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => [
                'quality'    => '70%',
                'upsize'     => true,
                'thumbnails' => [
                    [
                        'name'  => 'medium',
                        'scale' => '50%',
                    ],
                    [
                        'name'  => 'small',
                        'scale' => '25%',
                    ],
                    [
                        'name' => 'cropped',
                        'crop' => [
                            'width'  => '300',
                            'height' => '250',
                        ],
                    ],
                ],
            ],
            'order'    => 6,
        ],
        'image_alt'       => [
            'type'     => 'text',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 7,
        ],
        'author_id'       => [
            'type'     => 'text_area',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 8,
        ],
        'seo_title'       => [
            'type'     => 'text',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 9,
        ],
        'seo_description' => [
            'type'     => 'text',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => '',
            'order'    => 10,
        ],
        'no_index'        => [
            'type'     => 'checkbox',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => [
                'on' => 'Yes',
                'off' => 'No',
            ],
            'order'    => 11,
        ],
        'no_follow'       => [
            'type'     => 'checkbox',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 1,
            'add'      => 1,
            'delete'   => 1,
            'details'  => [
                'on' => 'Yes',
                'off' => 'No',
            ],
            'order'    => 12,
        ],
        'created_at'      => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 1,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 13,
        ],
        'updated_at'      => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 1,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 14,
        ],
    ];

    /**
     *
     */
    public function createMenuItem()
    {
        $admin_menu = Menu::findOrFail(1);

        MenuItem::firstOrCreate([
            'menu_id'    => $admin_menu->id,
            'title'      => 'Pages',
            'url'        => '/admin/pages',
            'target'     => '_self',
            'icon_class' => 'voyager-file-text',
            'color'      => '#000000',
            'parent_id'  => null,
            'order'      => 9,
            'route'      => null,
            'parameters' => null,
        ]);
    }

    /**
     * @throws \Exception
     */
    public function deleteMenuItem()
    {
        MenuItem::where([
            'title'      => 'Pages',
            'url'        => '/admin/pages',
        ])->delete();
    }
}