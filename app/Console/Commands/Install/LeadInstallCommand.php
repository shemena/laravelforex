<?php

namespace App\Console\Commands\Install;

use App\Console\Commands\Install\Core\HasInstallAndRevertMethods;
use App\Console\Commands\Install\Core\MustInstall;
use App\Console\Commands\Install\Core\MustRevert;
use App\Models\Lead;
use App\Models\Menu;
use App\Models\MenuItem;
use Illuminate\Console\Command;

class LeadInstallCommand extends Command implements MustInstall, MustRevert
{
    use HasInstallAndRevertMethods;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:leads {--revert}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use this command only if table exist in database';

    protected static $table       = 'leads';
    protected static $model       = Lead::class;
    protected        $icon        = 'voyager-people';
    protected static $field_rules = [
        'id'         => [
            'type'     => 'number',
            'required' => 1,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 1,
        ],
        'name'       => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 2,
        ],
        'last_name'  => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 0,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 3,
        ],
        'email'      => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 4,
        ],
        'phone'      => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 0,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 5,
        ],
        'country'    => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 6,
        ],
        'language'   => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 7,
        ],
        'in_crm'     => [
            'type'     => 'checkbox',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => [
                'on'  => 'Yes',
                'off' => 'No',
            ],
            'order'    => 8,
        ],
        'promo'      => [
            'type'     => 'number',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 9,
        ],
        'source'     => [
            'type'     => 'text',
            'required' => 1,
            'browse'   => 1,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 10,
        ],
        'created_at' => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 0,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 11,
        ],
        'updated_at' => [
            'type'     => 'timestamp',
            'required' => 0,
            'browse'   => 0,
            'read'     => 1,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 12,
        ],
    ];

    /**
     *
     */
    public function createMenuItem()
    {
        $admin_menu = Menu::findOrFail(1);
        $leads_menu = \App\Models\MenuItem::firstOrCreate([
            'menu_id'    => $admin_menu->id,
            'title'      => 'Leads',
            'url'        => '',
            'target'     => '_self',
            'icon_class' => 'voyager-people',
            'color'      => '#000000',
            'parent_id'  => null,
            'order'      => 2,
            'route'      => null,
            'parameters' => null,
        ]);

        MenuItem::firstOrCreate([
            'menu_id'    => $admin_menu->id,
            'title'      => 'Live',
            'url'        => '/admin/leads',
            'target'     => '_self',
            'icon_class' => 'voyager-person',
            'color'      => '#000000',
            'parent_id'  => $leads_menu->id,
            'order'      => 1,
            'route'      => null,
            'parameters' => null,
        ]);
    }

    /**
     * @throws \Exception
     */
    public function deleteMenuItem()
    {
        MenuItem::where([
            'title' => 'Live',
            'url'   => '/admin/leads',
        ])->delete();
    }
}