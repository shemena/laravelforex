<?php
namespace App\Console\Commands\Install;

use App\Console\Commands\Install\Core\HasInstallAndRevertMethods;
use App\Console\Commands\Install\Core\MustInstall;
use App\Console\Commands\Install\Core\MustRevert;
use App\Models\Menu;
use App\Models\MenuItem;
use App\Models\Localization;
use Illuminate\Console\Command;
use TCG\Voyager\Models\DataRow;

class LocalizationInstallCommand extends Command implements MustInstall, MustRevert
{
    use HasInstallAndRevertMethods;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:localization {--revert}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use this command only if table exist in database';

    protected static $table       = 'localizations';
    protected static $model       = Localization::class;
    protected static $field_rules = [
        'id'                  => [
            'type'         => 'number',
            'display_name' => 'Id',
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'details'      => '',
            'order'        => 1,
        ],
        'iso_code'            => [
            'type'         => 'text',
            'display_name' => 'Iso Code',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 0,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'details'      => '',
            'order'        => 2,
        ],
        'name'                => [
            'type'         => 'text',
            'display_name' => 'Name',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 0,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'details'      => '',
            'order'        => 3,
        ],
        'script'              => [
            'type'         => 'text',
            'display_name' => 'Script',
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'details'      => '',
            'order'        => 4,
        ],
        'native'              => [
            'type'         => 'text',
            'display_name' => 'Native',
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'details'      => '',
            'order'        => 5,
        ],
        'regional'            => [
            'type'         => 'text',
            'display_name' => 'Regional',
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'details'      => '',
            'order'        => 6,
        ],
        'enable'              => [
            'type'         => 'checkbox',
            'display_name' => 'Enable For Website',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 0,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'details'      => [
                'on'  => 'Yes',
                'off' => 'No',
            ],
            'order'        => 7,
        ],
        'status'              => [
            'type'         => 'checkbox',
            'display_name' => 'Enable For Content',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 0,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'details'      => [
                'on'  => 'Yes',
                'off' => 'No',
            ],
            'order'        => 8,
        ],
        'alphabet'            => [
            'type'         => 'text_area',
            'display_name' => 'Alphabet',
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'details'      => '',
            'order'        => 9,
        ],
        'created_at'      => [
            'type'     => 'timestamp',
            'display_name' => 'Created At',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 10,
        ],
        'updated_at'      => [
            'type'     => 'timestamp',
            'display_name' => 'Updated At',
            'required' => 0,
            'browse'   => 0,
            'read'     => 0,
            'edit'     => 0,
            'add'      => 0,
            'delete'   => 0,
            'details'  => '',
            'order'    => 11,
        ],
    ];

    /**
     *
     */
    public function createMenuItem()
    {
        $admin_menu = Menu::findOrFail(1);

        MenuItem::firstOrCreate([
            'menu_id'    => $admin_menu->id,
            'title'      => 'Localizations',
            'url'        => '/admin/localizations',
            'target'     => '_self',
            'icon_class' => 'voyager-logbook',
            'color'      => '#000000',
            'parent_id'  => null,
            'order'      => 21,
            'route'      => 'voyager.localizations.index',
            'parameters' => null,
        ]);
    }

    /**
     * @throws \Exception
     */
    public function deleteMenuItem()
    {
        MenuItem::where([
            'title'      => 'Localizations',
            'url'        => '/admin/localizations',
        ])->delete();
    }

    public function setRowsSettings()
    {
        foreach (static::$field_rules as $field => $rules) {
            DataRow::firstOrCreate(array_merge(static::$field_rules[$field], [
                'data_type_id' => $this->data_type->id,
                'field' => $field,
            ]));
        }
    }
}