<?php
namespace App\Services;

use GuzzleHttp\Client;

abstract class Service
{
    protected $http;
    protected $host;

    /**
     * Service constructor.
     * @param string $host
     */
    public function __construct(string $host)
    {
        $this->http = new Client([
            'base_uri' => $this->host = $host,
        ]);
    }
}