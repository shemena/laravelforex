<?php
namespace App\Services;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CrmService extends Service
{
    public  $request  = null;
    private $response = null;

    /**
     * CrmFGService constructor.
     */
    public function __construct()
    {
        parent::__construct(config('services.crm.host'));
    }

    /**
     * @param Request $request
     */
    public function sendLead(Request $request)
    {
        $this->request  = $this->http->post(
            config('services.crm.routes.lead.create'),
            [
                'json' => [
                    'first_name'   => $request->name,
                    'last_name'    => $request->last_name,
                    'phone_number' => $request->phone,
                    'email'        => $request->email,
                    'country'      => $request->country,
                    'promo_code'   => $request->promo,
                    'language'     => $this->setLanguage($request->language),
                    'address'      => '',
                ],
            ]
        );
        $this->response = $this->makeResponse();

        if ($this->response->isOk()) {
            $this->response->setData(
                array_merge($this->response->getData(true), [
                    'success'  => true,
                    'redirect' => $this->getRedirectUri(),
                ])
            );
        } else {
            $this->response->setData(
                array_merge($data = $this->response->getData(true), [
                    'success' => false,
                    'errors'  => [
                        'crm' => $data['message'],
                    ],
                ])
            );
        }
    }

    /**
     * @return null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Array of supported languages
     * @return array
     */
    public static function getSupportedLanguages(): array
    {
        return config('services.crm.supported_languages');
    }

    /**
     * Set language for crm
     * @param string $language
     * @return mixed|string
     */
    private function setLanguage($language = 'English'): string
    {
        return array_key_exists($language, $all = self::getSupportedLanguages()) ? $all[$language] : 'English';
    }

    /**
     * @return string
     */
    private function getRedirectUri()
    {
        $autoLoginLink = $this->response->getData(true)['autoLoginLink'];
        return $autoLoginLink ?? route('thank-you');
    }

    /**
     * @return JsonResponse
     */
    private function makeResponse(): JsonResponse
    {
        $response = json_decode($this->request->getBody()->getContents(), true);
        $status   = !is_null($response['autoLoginLink']) ? 200 : 400;

        return response()->json($response, $status);
    }
}