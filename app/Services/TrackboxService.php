<?php
namespace App\Services;

use App\Exceptions\JsonException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

class TrackboxService extends Service
{
    /**
     * TrackboxService constructor.
     */
    public function __construct()
    {
        parent::__construct(config('services.trackbox.host'));
    }

    /**
     * @param Request $request
     * @return TrackboxService
     */
    public function sendLead(Request $request) : self
    {
        try{
            $this->http->post(
                config('services.trackbox.routes.lead.send'),
                [
                    'form_params' => [
                        'ai'        => $request->ai,
                        'ci'        => $request->ci,
                        'gi'        => $request->gi,
                        'userip'    => $request->userip,
                        'firstname' => $request->name,
                        'lastname'  => $request->last_name,
                        'email'     => $request->email,
                        'phone'     => $request->phone,
                        'sub'       => $request->promo,
                        'password'  => '',
                        'MPC_1'     => '',
                        'MPC_2'     => $request->mpc_2,
                        'source'    => $request->source,
                    ],
                ]
            );
            return $this;
        } catch (ClientException $exception) {
            throw new JsonException([
                'success'   => false,
                'exception' => $exception,
            ], 400, 'Trackbox error!');
        }
    }
}