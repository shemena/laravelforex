<?php
namespace App\Services;

use App\Exceptions\JsonException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

class BackofficeService extends Service
{
    protected $response = null;

    /**
     * BackofficeService constructor.
     */
    public function __construct()
    {
        parent::__construct(config('services.backoffice.host'));
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function sendDemoLead(Request $request)
    {
        if(env('BACKOFFICE_ENABLED', false)){
            try{
                $this->response = $this->http->post(
                    config('services.backoffice.routes.demo-lead.create'),
                    [
                        'form_params' => [
                            'Name'               => $request->name,
                            'Email'              => $request->email,
                            'Country'            => $request->country,
                            'PhoneNum'           => $request->phone,
                            'SecurityKey'        => config('services.backoffice.security_key'),
                            'SetBalance'         => 1000,
                            'RegistrationSource' => config('services.backoffice.source'),
                        ],
                    ]
                );
                return true;
            }catch(ClientException $exception){
                throw new JsonException([
                    'success'   => false,
                    'exception' => $exception,
                ], 400, 'BackOffice error!');
            }
        }else{
            return false;
        }
    }
}