<?php
namespace App\Services;

use Illuminate\Support\Facades\Log;

class IpDataService extends Service
{
    protected $ip;

    /**
     * IpStackService constructor.
     */
    public function __construct()
    {
        parent::__construct(config('services.ipdata.host'));

        $this->ip = self::getIp();
    }

    /**
     * List of countries for ban
     * @return array
     */
    public function getBlackList() : array
    {
        return [
            'bulgaria',
        ];
    }

    /**
     * Getting request IP
     * @return string
     */
    public static function getIp()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    } else {
                        return '127.0.0.1';
                    }
                }
            }
        }
    }

    /**
     * Getting country by IP
     * @return mixed|null|string|string[]
     */
    public function getCountry()
    {
        try{
            $response = $this->http->request('GET', $this->ip . '/?api-key=' . config('services.ipdata.api_key'));
            $country = mb_strtolower(json_decode($response->getBody(),true)['country_name']);
            Log::info($this->ip . '-' . $country);
            return $country;
        }catch (\Exception $exception){
            Log::info($exception->getMessage());
            return '';
        }
    }
}