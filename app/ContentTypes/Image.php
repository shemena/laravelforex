<?php

namespace App\ContentTypes;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image as InterventionImage;
use TCG\Voyager\Http\Controllers\ContentTypes\Image as VoyagerImage;

class Image extends VoyagerImage
{
    public $file = null;

    /**
     * Image constructor.
     * @param UploadedFile $file
     * @param string $slug
     * @param array $options
     */
    public function __construct(UploadedFile $file, string $slug, array $options = [])
    {
        parent::__construct(request(), $slug, null, $options);
        $this->file = $file;
    }

    /**
     * @return mixed|string
     */
    public function handle()
    {
        return $this->save();
    }

    /**
     * @return string
     */
    public function save(): string
    {
        /*if ($this->is_animated_gif($file)) {
            Storage::disk(config('voyager.storage.disk'))->put($fullPath, file_get_contents($file), 'public');
            $fullPathStatic = $path.$filename.'-static.'.$file->getClientOriginalExtension();
            Storage::disk(config('voyager.storage.disk'))->put($fullPathStatic, (string) $image, 'public');
        } else {
            Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string) $image, 'public');
        }*/
        $image = $this->resize($this->file);
        $storage = Storage::disk(config('voyager.storage.disk'));
        $storage->put($path = $this->getPathToSave(), (string) $image, 'public');

        return $path;
    }

    /**
     * @param null $width
     * @param null $height
     * @return mixed
     */
    protected function resize($image, $width = null, $height = null)
    {
        //----------------------------------------------
        /*if (isset($this->options->thumbnails)) {
            foreach ($this->options->thumbnails as $thumbnails) {
                if (isset($thumbnails->name) && isset($thumbnails->scale)) {
                    $scale = intval($thumbnails->scale) / 100;
                    $thumb_resize_width = $resize_width;
                    $thumb_resize_height = $resize_height;

                    if ($thumb_resize_width != null && $thumb_resize_width != 'null') {
                        $thumb_resize_width = intval($thumb_resize_width * $scale);
                    }

                    if ($thumb_resize_height != null && $thumb_resize_height != 'null') {
                        $thumb_resize_height = intval($thumb_resize_height * $scale);
                    }

                    $image = InterventionImage::make($file)->resize(
                        $thumb_resize_width,
                        $thumb_resize_height,
                        function (Constraint $constraint) {
                            $constraint->aspectRatio();
                            if (isset($options->upsize) && !$this->options->upsize) {
                                $constraint->upsize();
                            }
                        }
                    )->encode($file->getClientOriginalExtension(), $resize_quality);
                } elseif (isset($thumbnails->crop->width) && isset($thumbnails->crop->height)) {
                    $crop_width = $thumbnails->crop->width;
                    $crop_height = $thumbnails->crop->height;
                    $image = InterventionImage::make($file)
                        ->fit($crop_width, $crop_height)
                        ->encode($file->getClientOriginalExtension(), $resize_quality);
                }

                Storage::disk(config('voyager.storage.disk'))->put(
                    $path.$filename.'-'.$thumbnails->name.'.'.$file->getClientOriginalExtension(),
                    (string) $image,
                    'public'
                );
            }
        }*/
        //------------------------------------------------
        $image = InterventionImage::make($image);

        if(is_null($this->options)){
            return $image;
        }

        if (is_null($width) && is_null($height)) {
            if (isset($this->options->resize) && (
                    isset($this->options->resize->width) || isset($this->options->resize->height)
                )) {
                if (isset($this->options->resize->width)) {
                    $width = $this->options->resize->width;
                }
                if (isset($this->options->resize->height)) {
                    $height = $this->options->resize->height;
                }
            } else {
                $width  = $image->width();
                $height = $image->height();
            }
        }

        $resize_quality = isset($options->quality) ? intval($this->options->quality) : 75;

        return $image->resize(
            $width,
            $height,
            function (Constraint $constraint)
            {
                $constraint->aspectRatio();
                if (isset($this->options->upsize) && !$this->options->upsize) {
                    $constraint->upsize();
                }
            }
        )->encode($this->file->getClientOriginalExtension(), $resize_quality);
    }

    /**
     * @return string
     */
    public function getPathToSave(): string
    {
        $extension       = $this->file->getClientOriginalExtension();
        $path_to_storage = $this->slug . '/' . date('FY') . '/';
        $name_to_save    = $this->generateFileName($this->file, $path_to_storage);

        return $path_to_storage . $name_to_save . '.' . $extension;
    }

    /**
     * @param $filename
     * @return bool
     */
    protected function is_animated_gif($filename)
    {
        $raw = file_get_contents($filename);

        $offset = 0;
        $frames = 0;
        while ($frames < 2) {
            $where1 = strpos($raw, "\x00\x21\xF9\x04", $offset);
            if ($where1 === false) {
                break;
            } else {
                $offset = $where1 + 1;
                $where2 = strpos($raw, "\x00\x2C", $offset);
                if ($where2 === false) {
                    break;
                } else {
                    if ($where1 + 8 == $where2) {
                        $frames++;
                    }
                    $offset = $where2 + 1;
                }
            }
        }

        return $frames > 1;
    }

    /**
     * @param UploadedFile $file
     * @param string $slug
     * @param array $options
     * @return string
     */
    public static function makeFrom(UploadedFile $file, string $slug, array $options = [])
    {
        $image = new self($file, $slug, $options);
        return $image->save();
    }
}