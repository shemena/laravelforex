<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class JsonException extends HttpException
{
    public $code;
    public $json = [];

    public function __construct(array $json = array(), $statusCode, $message = null, \Exception $previous = null, array $headers = array(), int $code = 0)
    {
        $this->code = $statusCode;
        $this->json = [
            'success' => $statusCode === 200 ? true : false,
            'errors'  => $json,
        ];
        parent::__construct($statusCode, $message, $previous, $headers, $code);
    }
}