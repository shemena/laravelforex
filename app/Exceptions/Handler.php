<?php

namespace App\Exceptions;

use App\Models\Document;
use App\Models\MenuItem;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\View;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        /**
         * Get all localized variants of current url
         */
        $urls = [];
        foreach(LaravelLocalization::getSupportedLocales() as $locale => $array){
            $urls[$locale] = LaravelLocalization::getLocalizedURL(
                $locale,
                LaravelLocalization::getNonLocalizedURL(url()->current())
            );
        }

        /**
         * Get documents for footer
         */
        $documents = Document::where('page', 'home')->orderBy('order')->get();

        View::share('language', LaravelLocalization::getCurrentLocale());
        View::share('localizedUrls', $urls);
        View::share('menu_header', MenuItem::forMenu('header')->get());
        View::share('menu_footer', MenuItem::forMenu('footer')->get());
        View::share('documents', $documents);

        return parent::render($request, $exception);
    }
}
