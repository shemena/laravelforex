<?php

namespace App\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class ByLocaleScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $locale = app()->getLocale();

        if ($locale !== config('voyager.multilingual.default')){
            $builder->whereHas('translations', function ($translation) use ($locale)
            {
                $translation->where([
                    'column_name' => 'title',
                    'locale'      => $locale,
                    [
                        'value',
                        '<>',
                        '',
                    ],
                ]);
            });
        }
    }
}