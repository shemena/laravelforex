<?php

namespace App\Models;

use App\Models\Scopes\InOrderScope;
use App\Models\Scopes\StatusScope;
use App\Traits\Models\HasScopes;
use TCG\Voyager\Models\Post;

class Document extends Post
{
    use HasScopes;

    public $timestamps      = true;

    protected $table        = 'documents';
    protected $translatable = [
        'title',
    ];

    protected static function boot()
    {
        parent::boot();

        self::bindGlobalScopes([
            new StatusScope(),
            new InOrderScope(),
        ]);
    }
}
