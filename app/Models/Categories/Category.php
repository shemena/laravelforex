<?php
namespace App\Models\Categories;

class Category extends \TCG\Voyager\Models\Category
{
    protected $table = 'categories';
}