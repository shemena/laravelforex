<?php

namespace App\Models;

use App\Models\Scopes\StatusScope;
use App\Traits\Models\HasImage;
use App\Traits\Models\HasLanguages;
use App\Traits\Models\HasScopes;
use App\Traits\Models\HasUrl;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Page extends \TCG\Voyager\Models\Page
{
    use HasLanguages;
    use HasUrl;
    use HasImage;
    use HasScopes;

    public $timestamps = true;

    protected $table        = 'pages';
    protected $translatable = [
        'title',
        'slug',
        'content',
        'image_alt',
        'seo_title',
        'seo_description',
    ];

    protected static function boot()
    {
        parent::boot();

        self::bindGlobalScopes([
            new StatusScope(),
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parent(){
        return $this->hasOne(self::class, 'parent_id', 'id');
    }

    /**
     * Get array of translated urls for current model
     * @return array
     */
    public function getLocalizedUrls(){

        $urls = [];

        foreach(LaravelLocalization::getSupportedLocales() as $locale => $array){
            $urls[$locale] = LaravelLocalization::getLocalizedURL($locale, '/' . $this->getTranslatedAttribute('slug', $locale));
        }

        return $urls;
    }
}
