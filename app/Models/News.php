<?php

namespace App\Models;

use App\Models\Scopes\InOrderScope;
use App\Models\Scopes\LatestScope;
use App\Models\Scopes\StatusScope;
use App\Traits\Models\HasImage;
use App\Traits\Models\HasLanguages;
use App\Traits\Models\HasScopes;
use App\Traits\Models\HasUrl;
use TCG\Voyager\Models\Post;

class News extends Post
{
    use HasLanguages;
    use HasUrl;
    use HasImage;
    use HasScopes;

    public $timestamps = true;

    protected $table = 'news';
    protected $translatable = [
        'title',
        'slug',
        'content',
        'excerpt',
        'seo_title',
        'seo_description',
    ];

    protected static function boot()
    {
        parent::boot();

        self::bindGlobalScopes([
            new StatusScope(),
            new LatestScope(),
        ]);
    }
}