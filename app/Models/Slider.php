<?php

namespace App\Models;

use App\Models\Scopes\ByLocaleScope;
use App\Models\Scopes\InOrderScope;
use App\Models\Scopes\StatusScope;
use App\Traits\Models\HasImage;
use App\Traits\Models\HasScopes;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Slider extends Model
{
    use HasScopes;
    use Translatable;
    use HasImage {
        HasImage::saveTranslations insteadof Translatable;
    }

    protected $table        = 'sliders';
    protected $translatable = [
        'title',
        'image',
        'image_alt',
        'h1',
        'h2',
        'button',
        'link',
    ];

    protected static function boot()
    {
        parent::boot();

        self::bindGlobalScopes([
            new StatusScope(),
            new InOrderScope(),
            new ByLocaleScope(),
        ]);
    }
}
