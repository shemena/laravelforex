<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DemoLead extends Model
{
    protected     $table      = 'demo_leads';
    protected     $guarded    = [''];

    public        $timestamps = true;
    public static $rules      = [
        'name'      => 'required|string|min:2|max:20',
        'last_name' => 'sometimes|string|min:2|max:100',
        'email'     => 'required|email|max:50',
        'phone'     => 'required|string|min:10|max:30',
        'country'   => 'required|string|min:3|max:50',
        'language'  => 'sometimes|string|min:2|max:2',
        'source'    => 'required|string|min:5|max:100',
    ];
}
