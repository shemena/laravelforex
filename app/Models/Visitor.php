<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $table = 'visitors';
    protected $guarded = [''];

    public function scopeInBlacklist($query)
    {
        return $query->where('in_blacklist', true);
    }

    public function scopeInWhitelist($query)
    {
        return $query->where('in_whitelist', true);
    }
}
