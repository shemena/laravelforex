<?php

namespace App\Models;

use App\Models\Scopes\StatusScope;
use App\Traits\Models\HasScopes;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Post;

class Landing extends Post
{
    use HasScopes;

    public $timestamps = true;

    protected        $table        = 'landings';
    protected static $view_path    = 'landings';
    protected        $translatable = [];

    /**
     * Landing constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->updateViewSettings();
    }

    /**
     * Getting list of views in 'landings' directory
     * @return array
     */
    public static function getViewsList(): array
    {
        $views = resource_path('views\\' . self::$view_path);

        if (!file_exists($views)) {
            mkdir($views);
            return [];
        } else {
            $list = [];
            foreach (glob($views . "\\*.blade.php") as $file) {
                $file            = str_replace($views . '\\', '', $file);
                $filename        = explode('.', $file)[0];
                $list[$filename] = $file;
            }
            return $list;
        }
    }

    /**
     * Generate url with domain for landing routes
     * @return string
     */
    public static function getDomain(): string
    {
        return str_replace('//', '//' . 'lp' . '.', env('APP_URL'));
    }

    /**
     * Set route key as 'slug' attribute
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get model by translated slug
     * @param $value
     * @return mixed
     */
    public function resolveRouteBinding($value)
    {
        return $this->where($this->getRouteKeyName(), $value)->firstOrFail();
    }

    /**
     * Getting path to landings view
     * @return string
     */
    public function getViewPath()
    {
        return self::$view_path . '.' . $this->view;
    }

    /**
     * Model booting method
     */
    protected static function boot()
    {
        parent::boot();

        self::bindGlobalScopes([
            new StatusScope(),
        ]);
    }

    /**
     * Updating data in DataRow model for Landing entity
     */
    protected function updateViewSettings()
    {
        $dataType = DataType::where([
            'name' => $this->table,
            'slug' => $this->table,
        ])->with([
            'rows' => function ($row)
            {
                $row->where('field', 'view');
            },
        ])->first();

        if (empty($dataType)) {
            return;
        }

        $dataRow = $dataType->rows->first();
        $dataRow->update([
            'details' => [
                'options' => self::getViewsList(),
            ],
        ]);
    }
}
