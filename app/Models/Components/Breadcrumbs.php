<?php

namespace App\Models\Components;

class Breadcrumbs
{
    protected $crumbs = [];

    /**
     * Breadcrumbs constructor.
     */
    public function __construct()
    {
        $this->crumbs[trans('titles.home')] = route('home');
    }

    /**
     * Getter $crumbs
     * @return array
     */
    public function getCrumbs(): array
    {
        return $this->crumbs;
    }

    /**
     * Add crumb item
     * @param string $title
     * @param $url
     */
    public function addCrumb(string $title, $url)
    {
        $this->crumbs[$title] = $url;
    }

    /**
     * Delete crumbs item
     * @param $title
     */
    public function deleteCrumb($title)
    {
        if(array_key_exists($title, $this->crumbs)){
            unset($this->crumbs[$title]);
        }
    }
}