<?php

namespace App\Models;

use App\Models\Scopes\StatusScope;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Models\HasScopes;

class Localization extends Model
{
    protected $table = 'localizations';

    use HasScopes;

    protected static function boot()
    {
        parent::boot();

        self::bindGlobalScopes([
            new StatusScope(),
        ]);
    }

}
