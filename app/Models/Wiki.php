<?php

namespace App\Models;

use App\Models\Scopes\StatusScope;
use App\Traits\Models\HasLanguages;
use App\Traits\Models\HasScopes;
use App\Traits\Models\HasUrl;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use TCG\Voyager\Models\Post;

class Wiki extends Post
{
    use HasLanguages;
    use HasUrl;
    use HasScopes;

    public $timestamps = true;

    protected $table = 'wiki';
    protected $translatable = [
        'term',
        'content',
        'slug',
        'seo_title',
        'seo_description',
    ];

    /**
     * Return sorted vocabulary of current alphabet
     * @param string $language
     * @return array
     */
    public static function vocabulary(string $language) : array
    {
        $alphabet = config('laravellocalization.supportedLocales.'.$language.'.alphabet');
        $filter_column = ($language == 'ar' || $language == 'zh') ? 'slug' : 'term';

        $terms = self::query()->orderBy($filter_column)->get();
        $vocabulary = [];

        foreach($alphabet as $letter){
            $vocabulary[$letter] = $terms->filter(
                function ($value, $key) use($letter,$filter_column) {
                    $first_letter = mb_strtolower(mb_substr($value->getTranslatedAttribute($filter_column), 0, 1));
                    return $first_letter === $letter;
                }
            );
        }

        return $vocabulary;
    }

    protected static function boot()
    {
        parent::boot();

        self::bindGlobalScopes([
            new StatusScope(),
        ]);
    }
}
