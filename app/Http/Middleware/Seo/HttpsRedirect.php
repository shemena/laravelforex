<?php

namespace App\Http\Middleware\Seo;

use Closure;

class HttpsRedirect
{
    /**
     * Handle an incoming request. Redirect form Http to Https | without www.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(env('APP_ENV') === 'local'){
            return $next($request);
        }

        $subDomain = array_first(explode('.', parse_url($request->url(), PHP_URL_HOST)));

        if($subDomain === 'www' || !$request->secure()){
            return redirect()->secure($request->getRequestUri());
        }

        return $next($request);
    }
}
