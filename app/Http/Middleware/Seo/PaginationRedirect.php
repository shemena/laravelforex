<?php
namespace App\Http\Middleware\Seo;

use Closure;
use Illuminate\Routing\Route;

class PaginationRedirect
{
    /**
     * Handle an incoming request. Redirect form Http to Https | without www.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(env('APP_ENV') === 'local'){
            return $next($request);
        }

        if($request->get('page') === 1){
            return redirect()->secure($request->getRequestUri());
        }

        return $next($request);
    }
}