<?php

namespace App\Http\Middleware\Seo;

use Closure;

class UpperCaseRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(env('APP_ENV') === 'local'){
            return $next($request);
        }

        if((bool) preg_match('/[A-Z]/', $current_url = $request->url())){
            return redirect()->secure(strtolower($current_url));
        }

        return $next($request);
    }
}
