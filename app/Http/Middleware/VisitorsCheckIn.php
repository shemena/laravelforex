<?php
namespace App\Http\Middleware;

use Closure;
use App\Models\Visitor;
use App\Services\IpDataService;
use hisorange\BrowserDetect\Facade as Browser;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class VisitorsCheckIn
{
    protected $ip;
    protected $in_blacklist = false;
    protected $browser;
    protected $uniqid;
    protected $referer;
    protected $device;

    /**
     * Visits constructor.
     */
    public function __construct()
    {
        if (env('VISITORS_CHECKIN', true)) {
            $this->IpDataService = new IpDataService();
            $this->ip            = $this->IpDataService->getIp();
            $this->browser       = $this->getBrowser();
            $this->uniqid        = uniqid('', true);
            $this->device        = $this->getDevice();
            $this->referer       = $this->getReferer();
        }
    }

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!env('VISITORS_CHECKIN')) {
            return $next($request);
        }

        $this->useLevel(env('VISITORS_CHECKIN_LEVEL'));

        $uniqid = $request->cookie('visitor');

        if (empty($uniqid)) {
            setcookie('visitor', $this->uniqid, 0, '', '', $secure = true, $httponly = true);
            $this->createVisitor();
        } else {
            $this->updateVisitor($uniqid);
        }

        return $next($request);
    }

    /**
     * Get blacklist collection
     * @return Collection
     */
    public function getBlacklist(): Collection
    {
        return Visitor::inBlackList()->pluck('ip');
    }

    /**
     * Get whitelist collection
     * @return Collection
     */
    public function getWhitelist(): Collection
    {
        return Visitor::inWhiteList()->pluck('ip');
    }

    /**
     * Getting info about visitor browser data
     * @return null
     */
    protected function getBrowser()
    {
        if (Browser::isBot()) {
            return 'Bot';
        }else{
            return Browser::browserName();
        }
    }

    /**
     * Getting visitor device type
     * @return string
     */
    protected function getDevice()
    {
        if(Browser::isMobile()){
            return 'mobile';
        }elseif (Browser::isTablet()){
            return 'tablet';
        }else{
            return 'desktop';
        }
    }

    /**
     * Getting visitor referer link
     * @return array|null|string
     */
    protected function getReferer()
    {
        $referer = request()->server('HTTP_REFERER');
        $host = config('app.url');

        if (empty($referer) || preg_match("#{$host}#", $referer)){
            return null;
        }

        return $referer;
    }

    /**
     * Check if visitor exist in blacklist
     * @return bool
     */
    protected function inBlackList(): bool
    {
        $inBlacklist = in_array($this->ip, $this->getBlacklist()->toArray());
        return  !$this->inWhiteList() ? $inBlacklist : false;
    }

    /**
     * Check if visitor exist in blacklist
     * @return bool
     */
    protected function inWhiteList(): bool
    {
        return in_array($this->ip, $this->getWhitelist()->toArray()) ? true : false;
    }

    /**
     * Create new visitor
     * @return Visitor
     */
    protected function createVisitor(): Visitor
    {
        return Visitor::create([
            'ip'           => $this->ip,
            'uniqid'       => $this->uniqid,
            'count_visits' => 1,
            'browser'      => $this->browser,
            'referer'      => $this->referer,
            'device'       => $this->device,
            'country'      => $this->IpDataService->getCountry(),
        ]);
    }

    /**
     * Search visitor by uniqid and update it
     * @param string $uniqid
     * @return bool
     */
    protected function updateVisitor(string $uniqid)
    {
        $visitor            = Visitor::where('uniqid', $uniqid)->first();
        $this->in_blacklist = $visitor->count_visits > 400 ? true : $this->in_blacklist;

        if (!empty($visitor)) {
            return $visitor->update([
                'browser'      => $this->browser,
                'ip'           => $this->ip,
                'device'       => $this->device,
                'count_visits' => $visitor->count_visits + 1,
                'in_blacklist' => $this->in_blacklist,
            ]);
        } else {
            Log::info('Visitor with uniqid ' . $uniqid . ' not found');
        }
    }

    /**
     * @param string $level
     */
    protected function useLevel(string $level = 'green')
    {
        switch ($level){
            case 'green':
                if(Browser::isBot()){
                    $this->in_blacklist = true;
                }
                break;
            case 'yellow':
                if($this->inBlackList()){
                    abort(403);
                }
                break;
            case 'red':
                if(Browser::isBot() || $this->inBlackList()){
                    abort(403);
                }
                break;
            default:
                if(Browser::isBot()){
                    $this->in_blacklist = true;
                }
                break;
        }
    }
}