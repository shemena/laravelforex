<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController extends Controller
{
    public function index(Request $request): View
    {
        $slider = Slider::all();
        $news   = News::paginate(4);

        return view('home.index', [
            'request'    => $request,
            'slider'     => $slider,
            'news'       => $news,
            'socketRoom' => 'home',
        ]);
    }
}
