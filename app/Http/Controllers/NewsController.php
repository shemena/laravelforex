<?php

namespace App\Http\Controllers;

use App\Traits\Controllers\HasBreadcrumbs;
use App\Traits\Controllers\HasPagination;
use Illuminate\Http\Request;
use App\Models\News;
use Illuminate\Support\Facades\View;

class NewsController extends Controller
{
    use HasPagination;

    /** Show all news posts on single page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function all(Request $request)
    {
        $news = News::paginate(9);

        if(!$this->isPageExist($news)){
            return redirect(route('news', [
                'page' => $news->lastPage(),
            ]), 302, [], true);
        }

        return view('news.all', [
            'request'    => $request,
            'news'       => $news,
            'socketRoom' => 'news',
        ]);
    }

    /**
     * Show one news post on single page
     * @param Request $request
     * @param News $news
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function one(Request $request, News $news)
    {
        View::share('localizedUrls', $news->getLocalizedUrls());

        return view('news.one', [
            'request'    => $request,
            'news'       => $news,
            'socketRoom' => 'news',
        ]);
    }
}

