<?php

namespace App\Http\Controllers;

use App\Exceptions\JsonException;
use App\Http\Requests\DemoRegisterForm;
use App\Http\Requests\RegisterForm;
use App\Models\DemoLead;
use App\Models\Lead;
use App\Services\BackofficeService;
use App\Services\CrmService;
use App\Services\TrackboxService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class LeadController extends Controller
{
    protected $trackboxService;
    protected $backOfficeService;

    /**
     * LeadController constructor.
     * @param TrackboxService $trackboxService
     */
    public function __construct(
        TrackboxService $trackboxService,
        BackofficeService $backOfficeService
    )
    {
        parent::__construct();
        $this->trackboxService   = $trackboxService;
        $this->backOfficeService = $backOfficeService;
    }

    /**
     * Show log in page content | Login action (redirect to BO maybe)
     * @return RedirectResponse
     */
    public function login() : RedirectResponse
    {
        return redirect()->away(config('services.backoffice.routes.login'));
    }

    /**
     * Show sign up page content
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register(Request $request): View
    {
        return view('lead.live-register', [
            'request'    => $request,
            'socketRoom' => 'registration',
        ]);
    }

    /**
     * Show page content gor registering demo account
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function demoRegister(Request $request): View
    {
        return view('lead.demo-register', [
            'request'    => $request,
            'socketRoom' => 'demo-registration',
        ]);
    }

    /**
     * Creating new lead
     * @param RegisterForm $request
     * @return JsonResponse
     */
    public function create(RegisterForm $request): JsonResponse
    {
        $request->merge(['language' => $this->language]);
        $response = $this->sendToCrm($request);

        if($response->isOk()){
            $request->merge(['in_crm' => true]);
            $this->saveToDatabase($request);
        }

        return $response;
    }

    /**
     * Creating new demo lead
     * @param DemoRegisterForm $request
     * @return JsonResponse
     */
    public function createDemo(DemoRegisterForm $request): JsonResponse
    {
        try{

            $in_backoffice = $this->backOfficeService->sendDemoLead($request);
            $in_database   = $this->saveToDatabaseDemo($request);

            return response()->json([
                'success'       => true,
                'in_backoffice' => $in_backoffice,
                'in_database'   => $in_database,
                'redirect'      => route('thank-you'),
            ], 200);

        }catch (JsonException $exception){
            return response()->json($exception, $exception->getCode());
        }
    }

    /**
     * Create new lead via Trackbox system
     * @param RegisterForm $request
     * @return JsonResponse
     */
    public function createViaTrackbox(RegisterForm $request): JsonResponse
    {
        try{

            $in_trackbox = $this->sendToTrackbox($request);
            $in_database = $this->saveToDatabase($request);

            return response()
                    ->json([
                        'success'     => true,
                        'redirect'    => route('thank-you'),
                        'in_trackbox' => $in_trackbox,
                        'in_database' => $in_database,
                    ], 200)
                    ->header('Access-Control-Allow-Origin', '*')
                    ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');

        }catch(JsonException $exception){
            return response()->json($exception->json, $exception->code);
        }
    }

    // =======================================================================================

    /**
     * Sending lead data to Crm
     * @param Request $request
     * @return LeadController
     */
    protected function sendToCrm(Request $request): JsonResponse
    {
        if ($crmEnable = env('CRM_ENABLED', true)) {
            $crm = new CrmService();
            $crm->sendLead($request);
            return $crm->getResponse();
        } else {
            return response()->json([
                'success'  => true,
                'redirect' => route('thank-you'),
                'crm'      => $crmEnable,
            ], 401);
        }
    }

    /**
     * Saving lead in Database
     * @param RegisterForm $request
     * @return Lead
     */
    protected function saveToDatabase(RegisterForm $request): Lead
    {
        return Lead::create($request->only(array_keys(Lead::$rules)));
    }

    /**
     * Saving Demo lead in Database
     * @param DemoRegisterForm $request
     * @return DemoLead
     */
    protected function saveToDatabaseDemo(DemoRegisterForm $request): DemoLead
    {
        return DemoLead::create($request->only(array_keys(DemoLead::$rules)));
    }

    /**
     * Send lead in Trackbox system
     * @param RegisterForm $request
     * @return bool
     */
    protected function sendToTrackbox(RegisterForm $request)
    {
        try{
            $this->trackboxService->sendLead($request);
            $request->merge([
                'in_crm' => true,
            ]);
            return true;
        }catch (JsonException $exception){
            return false;
        }
    }
}
