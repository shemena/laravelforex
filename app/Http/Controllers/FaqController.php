<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class FaqController extends Controller
{
    /**
     * Show all posts on single page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all(Request $request)
    {
        $faq = Faq::all();

        return view('faq.all', [
            'title'      => trans('titles.faq'),
            'request'    => $request,
            'faq'        => $faq,
            'socketRoom' => 'faq',
        ]);
    }

    /**
     * Show one faq post on single page
     * @param Request $request
     * @param Faq $faq
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function one(Request $request, Faq $faq)
    {
        View::share('localizedUrls', $faq->getLocalizedUrls());

        return view('faq.one', [
            'request'    => $request,
            'faq'        => $faq,
            'socketRoom' => 'faq',
        ]);
    }
}