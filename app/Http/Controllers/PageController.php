<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PageController extends Controller
{
    /**
     * Show single page
     * @param Request $request
     * @param Page $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function one(Request $request, Page $page)
    {
        View::share('localizedUrls', $page->getLocalizedUrls());

        return view('page.one', [
            'request'    => $request,
            'page'       => $page,
            'socketRoom' => $page->title,
        ]);
    }
}
