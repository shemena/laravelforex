<?php

namespace App\Http\Controllers\Voyager;

use App\ContentTypes\Image;
use Illuminate\Http\Request;

class VoyagerBaseController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function getContentBasedOnType(Request $request, $slug, $row, $options = null)
    {
        if ($row->type === 'image') {

            if($request->hasFile($row->field)){
                $locale = config('voyager.multilingual.default');
                $files = $request->file($row->field);
                if(array_key_exists($locale, $files)){
                    return Image::makeFrom($files[$locale], $slug);
                }
            }

            $request[$row->field] = null;
            return null;
        }

        return parent::getContentBasedOnType($request, $slug, $row, $options);
    }
}