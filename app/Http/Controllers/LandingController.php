<?php
namespace App\Http\Controllers;

use App\Models\Landing;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    /**
     * Show single landing page view
     * @param Request $request
     * @param Landing $landing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function one(Request $request, Landing $landing)
    {
        $this->updateVisits($request, $landing);

        return view($landing->getViewPath(), [
            'landing' => $landing,
            'request' => $request,
        ]);
    }

    /**
     * Put info to session about visits of current landing page
     * @param Request $request
     * @param Landing $landing
     */
    protected function updateVisits(Request $request, Landing $landing)
    {
        if(
            !$request->session()->has('visit_landing_' . $landing->id) &&
            !$request->session()->get('visit_landing_' . $landing->id) === true
        ){
            $request->session()->put('visit_landing_' . $landing->id, true);
            $landing->increment('visits');
        }
    }
}
