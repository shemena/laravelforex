<?php

namespace App\Http\Controllers;

use App\Models\Promotion;
use App\Traits\Controllers\HasPagination;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PromotionController extends Controller
{
    use HasPagination;

    /**
     * Show all promotions on single page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all(Request $request)
    {
        $promotions = Promotion::paginate(9);

        if(!$this->isPageExist($promotions)){
            return redirect(route('promotions', [
                'page' => $promotions->lastPage(),
            ]), 302, [], true);
        }

        return view('promotion.all', [
            'title'      => trans('titles.promotions'),
            'request'    => $request,
            'promotions' => $promotions,
            'socketRoom' => 'promotions',
        ]);
    }

    /**
     * Show one promotion on single page
     * @param Request $request
     * @param Promotion $promotion
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function one(Request $request, Promotion $promotion)
    {
        View::share('localizedUrls', $promotion->getLocalizedUrls());

        return view('promotion.one', [
            'request'    => $request,
            'promotion'  => $promotion,
            'socketRoom' => 'promotions',
        ]);
    }
}
