<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AffiliateController extends Controller
{
    public function index(Request $request)
    {
        return view('affiliate.index',[
            'request'    => $request,
            'socketRoom' => 'affiliate',
        ]);
    }
}
