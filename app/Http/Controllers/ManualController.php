<?php
namespace App\Http\Controllers;

use App\Models\Manual;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class ManualController extends Controller
{
    /**
     * Show all posts on single page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all(Request $request)
    {
        $manual = Manual::all();

        return view('manual.all', [
            'title'      => trans('titles.manual'),
            'request'    => $request,
            'manual'     => $manual,
            'socketRoom' => 'manual',
        ]);
    }

    /**
     * Show one manual post on single page
     * @param Request $request
     * @param Manual $manual
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function one(Request $request, Manual $manual)
    {
        View::share('localizedUrls', $manual->getLocalizedUrls());

        return view('manual.one', [
            'request'    => $request,
            'manual'     => $manual,
            'socketRoom' => 'manual',
        ]);
    }
}