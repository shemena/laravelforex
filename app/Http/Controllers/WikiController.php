<?php

namespace App\Http\Controllers;

use App\Models\Wiki;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class WikiController extends Controller
{
    /**
     * Show all wiki terms on single page
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all(Request $request)
    {
        $wiki = Wiki::vocabulary($this->language);

        return view('wiki.all', [
            'title'      => trans('titles.wiki'),
            'request'    => $request,
            'wiki'       => !empty($wiki) ? $wiki : '',
            'socketRoom' => 'wiki',
        ]);
    }

    /**
     * Show one wiki term on single page
     * @param Request $request
     * @param Wiki $wiki
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function one(Request $request, Wiki $wiki)
    {
        View::share('localizedUrls', $wiki->getLocalizedUrls());

        return view('wiki.one', [
            'request'    => $request,
            'wiki'       => $wiki,
            'socketRoom' => 'wiki',
        ]);
    }
}
