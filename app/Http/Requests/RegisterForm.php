<?php

namespace App\Http\Requests;

use App\Models\Lead;
use Illuminate\Foundation\Http\FormRequest;

class RegisterForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Lead::$rules;
    }

    /**
     * Get error messages content
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'      => trans('errors.field'),
            'last_name.required' => trans('errors.field'),
            'email.required'     => trans('errors.field'),
            'phone.required'     => trans('errors.field'),
            'country.required'   => trans('errors.field'),

            'email.unique'       => trans('errors.email'),
            'phone.unique'       => trans('errors.phone'),
        ];
    }
}
