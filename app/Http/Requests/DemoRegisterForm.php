<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DemoRegisterForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required',
            'email'     => 'required|unique:demo_leads|max:50',
            'phone'     => 'required|unique:demo_leads|max:30',
            'country'   => 'required',
        ];
    }

    /**
     * Get error messages content
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'      => trans_choice('validation.required', trans('validation.name')),
            'email.required'     => trans_choice('validation.required', trans('validation.email')),
            'phone.required'     => trans_choice('validation.required', trans('validation.phone')),
            'country.required'   => trans_choice('validation.required', trans('validation.country')),

            'email.unique'       => trans_choice('validation.unique', trans('validation.email')),
            'phone.unique'       => trans_choice('validation.unique', trans('validation.phone')),
        ];
    }
}
