<?php
namespace App\Traits\Models;

use App\ContentTypes\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

trait HasImage
{
    /**
     * Getter for attribute 'image'
     * @param $value
     * @return string
     */
    public function getImageAttribute($value)
    {
        if (method_exists($this, 'translatable') && array_key_exists('image', $this->getTranslatableAttributes())){
            $value = $this->getTranslatedAttribute($value);
        }

        $image_exist = Storage::disk('public')->exists($value);
        if($image_exist){
            return secure_url(str_replace('\\', '/', Storage::url($value)));
        }else{
            return $value;
        }
    }

    /**
     * Save translations.
     *
     * @param object $translations
     *
     * @return void
     */
    public function saveTranslations($translations)
    {
        foreach ($translations as $field => $locales) {
            foreach ($locales as $locale => $translation) {
                if($field === 'image'){
                    $imagePath = $this->savedImagePath(request(), $this->getTable(), 'image', $translation->getlocale());
                    if(!empty($imagePath)){
                        $translation->$field = $imagePath;
                    }
                }
                $translation->save();
            }
        }
    }

    /**
     * Get path of saved to storage image
     * @param Request $request
     * @param string $slug
     * @param string $fieldName
     * @param $locale
     * @return string
     */
    protected function savedImagePath(Request $request, string $slug, string $fieldName, $locale)
    {
        if($request->hasFile($fieldName)){
            $files = $request->file($fieldName);
            if(array_key_exists($locale, $files)){
                return Image::makeFrom($files[$locale], $slug);
            }
        }

        return '';
    }
}