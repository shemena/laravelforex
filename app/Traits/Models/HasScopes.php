<?php
namespace App\Traits\Models;

use Illuminate\Support\Facades\Route;
use Illuminate\Database\Eloquent\Concerns\HasGlobalScopes;
use Barryvdh\Debugbar\Facade as Debugbar;

trait HasScopes
{
    use HasGlobalScopes;

    protected static function bindGlobalScopes(array $scopes)
    {
        if (!is_null($route = Route::current())) {
            $adminPrefix =  empty(config('voyager.user.redirect')) ?
                            empty(config('voyager.prefix')) ? '/admin' : '/' . config('voyager.prefix') :
                            config('voyager.user.redirect');
            $isAdminPanel = $route->getPrefix() !== $adminPrefix ? true : false;
            if ($isAdminPanel) {
                foreach ($scopes as $scope) {
                    static::addGlobalScope($scope);
                }
            }
        }
    }
}