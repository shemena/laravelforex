<?php

namespace App\Traits\Models;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

trait HasLanguages
{
    public $language;

    /**
     * MultiLang constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->language = LaravelLocalization::getCurrentLocale();
    }

    /**
     * Get array of translated urls for current model
     * @return array
     */
    public function getLocalizedUrls(){

        $urls = [];

        foreach(LaravelLocalization::getSupportedLocales() as $locale => $array){
            $urls[$locale] = LaravelLocalization::getLocalizedURL(
                $locale,
                $this->table . '/' . $this->getTranslatedAttribute('slug', $locale)
            );
        }

        return $urls;
    }
}