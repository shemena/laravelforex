<?php

namespace App\Traits\Models;

use App\Models\Categories\Category;
use App\Models\Translation;
use Illuminate\Database\Eloquent\Model;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

trait HasUrl
{
    /**
     * Set route key as 'slug' attribute
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get model by translated slug
     * @param $value
     * @return mixed
     */
    public function resolveRouteBinding($value)
    {
        if($this->language === config('voyager.multilingual.default')){
            return $this->where($this->getRouteKeyName(), $value)->first();
        }else{
            $id = Translation::select('foreign_key')
            ->where([
                'column_name' => 'slug',
                'table_name'  => $this->getTable(),
                'value'       => $value,
            ])
            ->firstOrFail()
            ->foreign_key;

            return $this->where([
                'id'     => $id,
                'status' => 1,
            ])->first();
        }
    }

    /**
     * Getting url attribute using slug
     * @param $value
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getUrlAttribute($value){
        // If model is category
        if (is_subclass_of($this, Category::class)) {
            $url = LaravelLocalization::getLocalizedUrl(
                $this->language,
                explode('_', $this->table)[0] .
                '/' .
                $this->getTranslatedAttribute('slug', $this->language)
            );
        }
        elseif (is_subclass_of($this, Model::class)) {
            // If model has category
            if($this->category()->exists()){
                $url = LaravelLocalization::getLocalizedUrl(
                    $this->language,
                    $this->table .
                    '/' .
                    $this->category->slug .
                    '/' .
                    $this->getTranslatedAttribute('slug', $this->language)
                );
                // If model has not category
            }else{
                $url = LaravelLocalization::getLocalizedUrl(
                    $this->language,
                    $this->table .
                    '/' .
                    $this->getTranslatedAttribute('slug', $this->language)
                );
            }
        } else {
            return;
        }
        return $url;
    }
}
