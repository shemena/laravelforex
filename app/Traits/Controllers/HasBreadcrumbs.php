<?php
namespace App\Traits\Controllers;

use App\Models\Components\Breadcrumbs;
use Illuminate\Support\Facades\View;

trait HasBreadcrumbs
{
    protected $breadcrumbs;

    protected function shareBreadcrumbs()
    {
        if($this->breadcrumbs instanceof Breadcrumbs){
            View::share('breadcrumbs', $this->breadcrumbs->getCrumbs());
        }
    }
}