<?php
namespace App\Traits\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

trait HasPagination
{
    /**
     * @param LengthAwarePaginator $paginator
     * @return bool
     */
    public function isPageExist(LengthAwarePaginator $paginator) : bool
    {
        if($paginator->currentPage() > $paginator->lastPage()){
            return false;
        }else{
            return true;
        }
    }
}